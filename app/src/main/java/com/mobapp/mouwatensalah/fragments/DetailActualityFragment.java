package com.mobapp.mouwatensalah.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mobapp.mouwatensalah.R;
import com.mobapp.mouwatensalah.model.News;
import com.mobapp.mouwatensalah.tools.Const;
import com.mobapp.mouwatensalah.tools.Languages;
import com.mobapp.mouwatensalah.tools.LhachTypeFace;
import com.mobapp.mouwatensalah.tools.LhachTypeStyle;
import com.mobapp.mouwatensalah.tools.LocallyFiles;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

// TODO: Auto-generated Javadoc

/**
 * The Class DetailActualityFragment.
 */
@SuppressLint("SetJavaScriptEnabled")
public class DetailActualityFragment extends Fragment {

    /**
     * The news.
     */
    private News news;

    /**
     * The description.
     */
    private WebView description;

    /**
     * The news_img.
     */
    private ImageView news_img;

    /**
     * declaration text views.date title
     */
    private TextView date, title;

    /**
     * Gets the single instance of DetailActualityFragment.
     *
     * @param value    the value
     * @param mContext the m context
     * @return single instance of DetailActualityFragment
     */
    public static DetailActualityFragment getInstance(News value,
                                                      Context mContext) {
        DetailActualityFragment mFragment = new DetailActualityFragment();
        mFragment.setNews(value);
        Bundle args = new Bundle();
        args.putString("name", value.getId());
        mFragment.setArguments(args);
        Gson g = new Gson();
        String sharedArtist = g.toJson(value);
        new LocallyFiles(mContext).saveLocallyFile("article" + value.getId(),
                sharedArtist);
        return mFragment;
    }

    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    @SuppressWarnings("deprecation")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.detail_news_fragment, container,
                false);
        LocallyFiles mLocallyFiles = new LocallyFiles(getActivity());

        if (news == null) {
            String name = getArguments().getString("name");
            Log.d("news_id", "___" + name);
            Gson mGson = new Gson();
            String sharedArtist = mLocallyFiles.getFileContentText("article"
                    + name);
            news = mGson.fromJson(sharedArtist, News.class);
            Log.d("news_id", "article" + name);
        }

        news_img = (ImageView) mView.findViewById(R.id.img);
        date = (TextView) mView.findViewById(R.id.date);
        title = (TextView) mView.findViewById(R.id.description);

        LhachTypeFace.setOoredoo(date, 0, LhachTypeStyle.REGULAR);
        LhachTypeFace.setOoredoo(title, 0, LhachTypeStyle.REGULAR);


        description = (WebView) mView.findViewById(R.id.newsContent);

        // define the text TypeFace

        // ***
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getActivity()).defaultDisplayImageOptions(DisplayImageOptions.createSimple()).build();

        imageLoader.init(config);
        imageLoader.displayImage(getNews().getUrl_Image(), news_img,
                DisplayImageOptions.createSimple());
        String htmlData;

        htmlData = getNews().getContent();
        date.setText(news.getDate());
        title.setText(news.getTitle());

        htmlData = htmlData.replace("\\/", "/");
        // htmlData = htmlData.replace("font-size:12px;", "font-size:14px;");

        // webView settings
        WebSettings webViewSettings = description.getSettings();
        webViewSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webViewSettings.setJavaScriptEnabled(true);
        webViewSettings.setBuiltInZoomControls(false);
        webViewSettings.setDefaultTextEncodingName("utf-8");
        webViewSettings.setPluginState(PluginState.ON);
        description.loadDataWithBaseURL(null, htmlData, "text/html",
                "UTF-8", null);
        //description.loadData(htmlData, "text/html", "UTF-8");

        return mView;
    }

    /**
     * Url correct.
     *
     * @param url the url
     * @return the string
     */
    public String urlCorrect(String url) {

        url.replace("\\/", "/");

        return url;
    }

    /**
     * Gets the news.
     *
     * @return the news
     */
    public News getNews() {
        return news;
    }

    /**
     * Sets the news.
     *
     * @param news the new news
     */
    public void setNews(News news) {
        this.news = news;
    }

}
