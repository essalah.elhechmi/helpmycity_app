package com.mobapp.mouwatensalah.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mobapp.mouwatensalah.DisplayReclamationActivity;
import com.mobapp.mouwatensalah.R;
import com.mobapp.mouwatensalah.database.repository.UserRepository;
import com.mobapp.mouwatensalah.model.User;
import com.mobapp.mouwatensalah.tools.Const;
import com.mobapp.mouwatensalah.tools.Languages;
import com.mobapp.mouwatensalah.tools.LhachTypeFace;
import com.mobapp.mouwatensalah.tools.LhachTypeStyle;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by salah on 20/04/2015.
 */
public class FragmentProfil extends Fragment {
    // the fragment support view
    private View rootView;

    // declaration views
    private TextView screenName, userName, lastName, email;
    private Button seeMyPics;
    private User mUser;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profil, null);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // init lbl's
        screenName = (TextView) view.findViewById(R.id.lbl1);
        userName = (TextView) view.findViewById(R.id.lbl2);
        lastName = (TextView) view.findViewById(R.id.lbl3);
        email = (TextView) view.findViewById(R.id.lbl4);

        seeMyPics = (Button) view.findViewById(R.id.seeMyPicsBtn);

        if (Languages.getLocalLanguage(getActivity()).equals(Const.FR)) {
            LhachTypeFace.setOoredoo(seeMyPics, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(screenName, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(userName, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(lastName, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(email, 0, LhachTypeStyle.REGULAR);
        } else {
            LhachTypeFace.setOoredooAr(seeMyPics, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(screenName, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(userName, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(lastName, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(email, 0, LhachTypeStyle.REGULAR);
        }

        // listen the click event
        seeMyPics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(getActivity(), DisplayReclamationActivity.class);
                startActivity(mIntent);
            }
        });

        new UserRepository(getContext()).getActiveUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<User>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(User user) {
                        mUser = user;
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        init();
                    }
                });


    }

    private void init() {
        screenName.setText(mUser.getUsername());
        userName.setText(mUser.getName());
        lastName.setText(mUser.getLast_name());
        email.setText(mUser.getEmail());
    }
}