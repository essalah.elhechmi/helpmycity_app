package com.mobapp.mouwatensalah.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mobapp.mouwatensalah.EventDetailActivity;
import com.mobapp.mouwatensalah.R;
import com.mobapp.mouwatensalah.adapter.EventAdapter;
import com.mobapp.mouwatensalah.model.Event;
import com.mobapp.mouwatensalah.model.News;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventFragment extends Fragment {

    /**
     * Adapter for events list
     */
    private EventAdapter eventAdapter;

    /**
     * declation of listView
     */
    private ListView mListView;

    /**
     * The eventss list.
     */
    private ArrayList<Event> eventsList;


    public EventFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_event, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mListView = (ListView) view.findViewById(R.id.listView);

        eventsList = Event.getEvents(getActivity());

        eventAdapter = new EventAdapter(getActivity(), R.layout.eventi_item, eventsList);

        mListView.setAdapter(eventAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent mIntent = new Intent(getActivity(), EventDetailActivity.class);
                mIntent.putExtra(EventDetailActivity.SHARE_KEY, eventsList.get(i));
                startActivity(mIntent);
            }
        });
    }
}
