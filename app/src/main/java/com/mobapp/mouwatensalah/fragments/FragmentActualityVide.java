package com.mobapp.mouwatensalah.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobapp.mouwatensalah.R;

/**
 * Created by ELHECHMI on 08/05/2015.
 */
public class FragmentActualityVide extends Fragment {

    public static final String KEY = "tag";

    private View rootView;
    private TextView tv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragement_vide, null);
        String tv_txt = getArguments().getString(KEY);

        tv = (TextView) rootView.findViewById(R.id.tv);
        if (tv_txt != null)
            tv.setText(tv_txt);
        return rootView;
    }
}
