package com.mobapp.mouwatensalah.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.mobapp.mouwatensalah.MainActivity;
import com.mobapp.mouwatensalah.R;
import com.mobapp.mouwatensalah.SendPictureActivity;
import com.mobapp.mouwatensalah.adapter.CategoriesAdapter;
import com.mobapp.mouwatensalah.model.Category;
import com.mobapp.mouwatensalah.tools.LhachTypeFace;
import com.mobapp.mouwatensalah.tools.LhachTypeStyle;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class ChoixFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {
    public static boolean isFirstStart = true;
    private RadioButton rBtn1, rBtn2, rBtn3, rBtn4, rBtn5, rBtn6,rBtn7, lastChecked;
    private TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7;

    private ListView categoriesList;

    /**
     * path of taked picture
     */
    public static Uri takedPictureUri;
    private CategoriesAdapter categoriesAdapter;

    private ImageView resultImageView;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ChoixFragment newInstance() {
        ChoixFragment fragment = new ChoixFragment();
        return fragment;
    }

    public ChoixFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_choix, container, false);
        categoriesList = (ListView) rootView.findViewById(R.id.categoriesList);

        ArrayList<Category> allCategories = Category.getCategoriesList(getActivity());
        if(allCategories != null)
            categoriesAdapter = new CategoriesAdapter(getActivity(),R.layout.category_item, allCategories);
        if(categoriesAdapter != null) {
            categoriesList.setAdapter(categoriesAdapter);
            categoriesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Category mCategory = categoriesAdapter.getItem(i);
                    SendPictureActivity.cat =  mCategory.get_id();
                    final File image = new File(getArguments().getString("image"));
                    ((MainActivity) getActivity()).startSendPictureActivity(image);

                }
            });
        }



        tv1 = (TextView) rootView.findViewById(R.id.tv1);
        tv2 = (TextView) rootView.findViewById(R.id.tv2);
        tv3 = (TextView) rootView.findViewById(R.id.tv3);
        tv4 = (TextView) rootView.findViewById(R.id.tv4);
        tv5 = (TextView) rootView.findViewById(R.id.tv5);
        tv6 = (TextView) rootView.findViewById(R.id.tv6);
        tv7 = (TextView) rootView.findViewById(R.id.tv7);

        rBtn1 = (RadioButton) rootView.findViewById(R.id.radioButton1);
        rBtn2 = (RadioButton) rootView.findViewById(R.id.radioButton2);
        rBtn3 = (RadioButton) rootView.findViewById(R.id.radioButton3);
        rBtn4 = (RadioButton) rootView.findViewById(R.id.radioButton4);
        rBtn5 = (RadioButton) rootView.findViewById(R.id.radioButton5);
        rBtn6 = (RadioButton) rootView.findViewById(R.id.radioButton6);
        rBtn7 = (RadioButton) rootView.findViewById(R.id.radioButton7);

        rBtn1.setOnCheckedChangeListener(this);
        rBtn2.setOnCheckedChangeListener(this);
        rBtn3.setOnCheckedChangeListener(this);
        rBtn4.setOnCheckedChangeListener(this);
        rBtn5.setOnCheckedChangeListener(this);
        rBtn6.setOnCheckedChangeListener(this);
        rBtn7.setOnCheckedChangeListener(this);

        LhachTypeFace.setOoredoo(tv1, 0, LhachTypeStyle.REGULAR);
        LhachTypeFace.setOoredoo(tv2,0, LhachTypeStyle.REGULAR);
        LhachTypeFace.setOoredoo(tv3,0, LhachTypeStyle.REGULAR);
        LhachTypeFace.setOoredoo(tv4,0, LhachTypeStyle.REGULAR);
        LhachTypeFace.setOoredoo(tv5,0, LhachTypeStyle.REGULAR);
        LhachTypeFace.setOoredoo(tv6,0, LhachTypeStyle.REGULAR);
        LhachTypeFace.setOoredoo(tv7,0, LhachTypeStyle.REGULAR);

        LhachTypeFace.setOoredoo(rBtn1,0, LhachTypeStyle.REGULAR);
        LhachTypeFace.setOoredoo(rBtn2,0, LhachTypeStyle.REGULAR);
        LhachTypeFace.setOoredoo(rBtn3,0, LhachTypeStyle.REGULAR);
        LhachTypeFace.setOoredoo(rBtn4,0, LhachTypeStyle.REGULAR);
        LhachTypeFace.setOoredoo(rBtn5,0, LhachTypeStyle.REGULAR);
        LhachTypeFace.setOoredoo(rBtn6,0, LhachTypeStyle.REGULAR);
        LhachTypeFace.setOoredoo(rBtn7,0, LhachTypeStyle.REGULAR);



        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!ChoixFragment.isFirstStart){
            ((MainActivity) getActivity()).changeFragment();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId()){
            case R.id.radioButton1:
                SendPictureActivity.cat = "1";
                break;

            case R.id.radioButton2:
                SendPictureActivity.cat = "2";
                break;

            case R.id.radioButton3:
                SendPictureActivity.cat = "3";
                break;

            case R.id.radioButton4:
                SendPictureActivity.cat = "4";
                break;

            case R.id.radioButton5:
                SendPictureActivity.cat = "5";
                break;

            case R.id.radioButton6:
                SendPictureActivity.cat = "6";
                break;

            case R.id.radioButton7:
                SendPictureActivity.cat = "7";
                break;
        }
        final File image = new File(getArguments().getString("image"));
        ((MainActivity) getActivity()).startSendPictureActivity(image);

    }
}