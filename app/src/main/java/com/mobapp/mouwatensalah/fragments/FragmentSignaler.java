package com.mobapp.mouwatensalah.fragments;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobapp.mouwatensalah.MainActivity;
import com.mobapp.mouwatensalah.R;
import com.mobapp.mouwatensalah.tools.CheckGeoLocalization;
import com.mobapp.mouwatensalah.tools.Const;
import com.mobapp.mouwatensalah.tools.Languages;
import com.mobapp.mouwatensalah.tools.LhachTypeFace;
import com.mobapp.mouwatensalah.tools.LhachTypeStyle;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by salah on 20/04/2015.
 */
public class FragmentSignaler extends Fragment {

    private File image;
    // declaration the screen view support
    private View rootView;

    private FrameLayout takePicture;
    private TextView lbl1;

    /**
     * path of taked picture
     */
    private static Uri takenPictureUri;

    private static final int TAKE_PICTURE = 1;
    private static final int PIC_CROP = 2;
    private static File imagePath;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_signaler, null);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ChoixFragment.isFirstStart = true;
        lbl1 = (TextView) view.findViewById(R.id.lbl1);

        takePicture = (FrameLayout) view.findViewById(R.id.takePictureBtn);

        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckGeoLocalization.isItActive(getActivity())) {
                    startCamera();
                }
            }
        });

        if (Languages.getLocalLanguage(getActivity()).equals(Const.FR)) {
            LhachTypeFace.setOoredoo(lbl1, 0, LhachTypeStyle.REGULAR);
//            LhachTypeFace.setOoredoo(takePicture, 0, LhachTypeStyle.REGULAR);
        } else {
            LhachTypeFace.setOoredooAr(lbl1, 0, LhachTypeStyle.REGULAR);
//            LhachTypeFace.setOoredooAr(takePicture, 0, LhachTypeStyle.REGULAR);
        }

        lbl1.setText(getResources().getString(R.string.signaler_title));


    }

    private Uri getImageFileUri() {

        // Create a storage directory for the images
        // To be safe(er), you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this

        imagePath = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), Const.DIRECTORY_NAME);
        String tag = "image";
        Log.d(tag, "Find " + imagePath.getAbsolutePath());
        if (!imagePath.exists()) {
            if (!imagePath.mkdirs()) {
                Log.d("CameraTestIntent", "failed to create directory");
                return null;
            } else {
                Log.d(tag, "create new Tux folder");
            }
        }

        // Create an image file name
        String timeStamp = null;
        //if(Languages.getLocalLanguage(getActivity()).equals(Const.FR))
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date().getTime());
//        else{
//            Languages.changeLocalLanguage(getActivity(), Const.FR);
//            Languages.loadLocaleLang(getActivity());
//            timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date().getTime());
//            Languages.changeLocalLanguage(getActivity(), Const.AR);
//            Languages.loadLocaleLang(getActivity());
//        }
        image = new File(imagePath, Const.DIRECTORY_NAME + "_" + timeStamp + ".jpg");
        if (!image.exists()) {
            try {
                image.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        //return image;

        // Create an File Uri
        return Uri.fromFile(image);
    }

    public void startCamera() {
        takenPictureUri = getImageFileUri();
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, takenPictureUri);


        getActivity().startActivityForResult(intent, TAKE_PICTURE);
    }

    public void updateViewWithChoiceFragment(File image) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        Bundle mBundle = new Bundle();
        mBundle.putString("image", image.getAbsolutePath());
        ChoixFragment mFragment = ChoixFragment.newInstance();
        mFragment.setArguments(mBundle);
        if (fragmentManager != null) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container, mFragment, mFragment.getTag())
                    .commit();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //data available only when default environment is setting. null for customize filename.
        MainActivity mainActivity = ((MainActivity) getActivity());
        if (requestCode == TAKE_PICTURE) {
            if (resultCode == Activity.RESULT_OK) {
                //performCrop();
                ChoixFragment.takedPictureUri = takenPictureUri;
                if (image == null) {
                    if (mainActivity != null)
                        mainActivity.updateViewWithChoiceFragment(new File(takenPictureUri.getPath()));
                } else if (mainActivity != null)
                    mainActivity.updateViewWithChoiceFragment(image);

            } else if (resultCode == Activity.RESULT_CANCELED) {
                // User cancelled the image capture
            }
        } else if (requestCode == PIC_CROP) {

            ChoixFragment.takedPictureUri = takenPictureUri;
            if (mainActivity != null)
                mainActivity.updateViewWithChoiceFragment(image);
            //((MainActivity) getActivity()).startSendPictureActivity(image);


        }

//        if (requestCode == TAKE_PICTURE) {
//            if (resultCode == Activity.RESULT_OK) {
//                ChoixFragment.takedPictureUri = takenPictureUri;
//                ((MainActivity)getActivity()).updateViewWithChoiceFragment(image);
//
//            } else if (resultCode == Activity.RESULT_CANCELED) {
//                // User cancelled the image capture
//            } else {
//                // Image capture failed, advise user
//            }
        // }
    }

    private void performCrop() {
        try {
            Uri picUri = Uri.fromFile(image);

            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            //set crop properties
            cropIntent.putExtra("scaleType", "centerCrop");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);

            //....snip....
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            //....snip....

            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
            cropIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
            //retrieve data on return
            cropIntent.putExtra("scale", true);
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        } catch (ActivityNotFoundException anfe) {
            //display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
