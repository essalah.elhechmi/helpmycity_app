package com.mobapp.mouwatensalah.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mobapp.mouwatensalah.MainActivity;
import com.mobapp.mouwatensalah.R;
import com.mobapp.mouwatensalah.tools.Const;
import com.mobapp.mouwatensalah.tools.Languages;
import com.mobapp.mouwatensalah.tools.LhachTypeFace;
import com.mobapp.mouwatensalah.tools.LhachTypeStyle;
import com.mobapp.mouwatensalah.tools.ShareSN;


public class FragmentSettings extends Fragment {

    /**
     * The default app SharedPreferences.
     */
    private static SharedPreferences preferences;

    /**
     * The SharedPreferences editor.
     */
    private static SharedPreferences.Editor editor;

    // declaration of the rootView contains all settings views
    private View rootView;

    // declaration of all view (buttons)
    private Button arBtn, frBtn, shareBtn;
    // declaration of CheckBox to change the alert status (activate/ deactivate)
    private CheckBox alertStatus;

    // declaration of labels
    private TextView lbl1, statusLbl, lbl2, lbl3;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_settings, null);
        return rootView;
    }

    private void initLblView() {
        // load app data with the new selected lng
        Languages.loadLocaleLang(getActivity());
        // update the btn background color
        updateLngBtnStatus();
        // update the textView
        shareBtn.setText(getResources().getString(R.string.settings_lbl4));
        statusLbl.setText(getResources().getString(R.string.settings_activate_lbl));
        lbl1.setText(getResources().getString(R.string.settings_lbl1));
        lbl2.setText(getResources().getString(R.string.settings_lbl2));
        lbl3.setText(getResources().getString(R.string.settings_lbl3));
        getActivity().setTitle(getResources().getString(R.string.title_activity_settings));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // send the token to server
        preferences = getActivity().getSharedPreferences(Const.PREF_FILE_NAME, Context.MODE_PRIVATE);

        // init view's
        alertStatus = (CheckBox) view.findViewById(R.id.alertcheckBox);
        arBtn = (Button) view.findViewById(R.id.btn_ar);
        frBtn = (Button) view.findViewById(R.id.btn_fr);
        shareBtn = (Button) view.findViewById(R.id.btn_share);
        lbl1 = (TextView) view.findViewById(R.id.lbl1);
        statusLbl = (TextView) view.findViewById(R.id.status_lbl);
        lbl2 = (TextView) view.findViewById(R.id.lbl2);
        lbl3 = (TextView) view.findViewById(R.id.lbl3);

        if (Languages.getLocalLanguage(getActivity()).equals(Const.FR)) {
            LhachTypeFace.setOoredoo(arBtn, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(frBtn, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(shareBtn, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(lbl1, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(statusLbl, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(lbl2, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(lbl3, 0, LhachTypeStyle.REGULAR);
        }else{
            LhachTypeFace.setOoredooAr(arBtn, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(frBtn, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(shareBtn, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(lbl1, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(statusLbl, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(lbl2, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(lbl3, 0, LhachTypeStyle.REGULAR);
        }
        // listen to the click event for each view

        // share btn listener
        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            new ShareSN(getActivity()).share("http://environnement.dev-appli-fb.com/", "Zoomi");
            }
        });

        // alert checkBox checked listener
        alertStatus.setChecked(preferences.getBoolean(Const.PREF_ALERT, true));
        alertStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                // initialize the alert parameter is activated or not

                editor = preferences.edit();
                if (isChecked)
                    editor.putBoolean(Const.PREF_ALERT, true);
                else
                    editor.putBoolean(Const.PREF_ALERT, false);
                editor.commit();



            }
        });

        // FR/ AR btns listener
        arBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // change the local lng on shared settings
                Languages.changeLocalLanguage(getActivity(), Const.AR);

                initLblView();

                ((MainActivity)getActivity()).updateMenuText();
            }
        });

        frBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // change the local lng on shared settings
                Languages.changeLocalLanguage(getActivity(), Const.FR);

                // update change
                initLblView();

                ((MainActivity)getActivity()).updateMenuText();

            }
        });

        updateLngBtnStatus();
    }

    private void updateLngBtnStatus() {
        if (Languages.getLocalLanguage(getActivity()).equals(Const.FR)) {
            frBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.active_lng_selector));
            arBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.gray_selector));
        } else {
            frBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.gray_selector));
            arBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.active_lng_selector));
        }
    }
}
