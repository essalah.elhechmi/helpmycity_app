package com.mobapp.mouwatensalah.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.mobapp.mouwatensalah.DetailActualityActivity;
import com.mobapp.mouwatensalah.R;
import com.mobapp.mouwatensalah.adapter.NewsAdapter;
import com.mobapp.mouwatensalah.model.News;
import com.mobapp.mouwatensalah.tools.Communication;
import com.mobapp.mouwatensalah.tools.LocallyFiles;

import org.json.JSONObject;

import java.util.ArrayList;

// TODO: Auto-generated Javadoc
//import com.mdweb.jazz2015.fragments.ActualityFragment.Callbacks;

/**
 * The Class ActualityFragment.
 */
public class ActualityFragment extends Fragment implements OnItemClickListener {

    /**
     * The Constant SHARED_News_OBJECT.
     */
    public static final String SHARED_News_OBJECT = "news.json";

    /**
     * The Constant SHARED_POSITION.
     */
    public static final String SHARED_POSITION = "position";

    /**
     * The m news adapter.
     */
    private NewsAdapter mNewsAdapter;

    /**
     * The m news list view.
     */
    private ListView mNewsListView;

    /**
     * The m locally files.
     */
    private LocallyFiles mLocallyFiles;

    /**
     * The mnews list.
     */
    private ArrayList<News> mnewsList;

    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.list_fragment, container, false);
        return mView;
    }

    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onViewCreated(android.view.View, android.os.Bundle)
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mNewsListView = (ListView) view.findViewById(R.id.list);
        mLocallyFiles = new LocallyFiles(getActivity());


        mnewsList = News.getListNews(getActivity());
        if (mnewsList != null && mnewsList.size() > 0) {
            mNewsAdapter = new NewsAdapter(getActivity(),
                    R.layout.news_item, mnewsList);
            mNewsListView.setAdapter(mNewsAdapter);
        }

        mNewsListView.setOnItemClickListener(this);

    }

    /* (non-Javadoc)
     * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
     */
    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

        Intent mIntent = new Intent(getActivity(),
                DetailActualityActivity.class);

        mIntent.putExtra(SHARED_POSITION, mnewsList.get(arg2).getId());
        getActivity().startActivity(mIntent);

    }

}