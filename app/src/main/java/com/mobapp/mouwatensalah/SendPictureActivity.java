package com.mobapp.mouwatensalah;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.location.Location;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.Geofence;
import com.google.gson.Gson;
import com.mobapp.mouwatensalah.adapter.CategoriesSpinnerAdapter;
import com.mobapp.mouwatensalah.adapter.DegreAdapter;
import com.mobapp.mouwatensalah.database.AppDatabase;
import com.mobapp.mouwatensalah.database.repository.ReclamationRepository;
import com.mobapp.mouwatensalah.database.repository.UserRepository;
import com.mobapp.mouwatensalah.fragments.ChoixFragment;
import com.mobapp.mouwatensalah.model.ApiResponse;
import com.mobapp.mouwatensalah.model.Category;
import com.mobapp.mouwatensalah.model.Reclamation;
import com.mobapp.mouwatensalah.model.User;
import com.mobapp.mouwatensalah.tools.CircleBitmapDisplayer;
import com.mobapp.mouwatensalah.tools.Communication;
import com.mobapp.mouwatensalah.tools.Const;
import com.mobapp.mouwatensalah.tools.Languages;
import com.mobapp.mouwatensalah.tools.LhachToast;
import com.mobapp.mouwatensalah.tools.LhachTypeFace;
import com.mobapp.mouwatensalah.tools.LhachTypeStyle;
import com.mobapp.mouwatensalah.tools.RoundedDrawableStroke;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import io.nlopez.smartlocation.OnActivityUpdatedListener;
import io.nlopez.smartlocation.OnGeofencingTransitionListener;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.geofencing.model.GeofenceModel;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class SendPictureActivity extends AppCompatActivity implements View.OnClickListener, OnLocationUpdatedListener, OnActivityUpdatedListener, OnGeofencingTransitionListener {

    /**
     * The default options.
     */
    private DisplayImageOptions defaultOptions;

    /**
     * The config.
     */
    private ImageLoaderConfiguration config;

    /**
     * The image loader.
     */
    private ImageLoader imageLoader;


    public static final String NOTIFY = "1";
    public static final String FELICITY = "3";
    public static final String SUGGESTED = "2";

    /**
     * path of taken picture
     */
    public File takenPictureFile;
    private ImageView resultImageView;

    private Spinner categorieSpinner, problemtypespinner;
    private EditText comment;
    private Button sendPictureBtn;

    private boolean isCapturingLocation = false;
    private boolean userWantsLocation = false;

    private ProgressBar progressBar;

    private Location location;
    private Reclamation reclamation;

    public static String cat = "";

//    private HashMap<String, Category> categoriesWillBeDisplay = new HashMap<>();

    public static String RECLAMATION_TYPE = NOTIFY;
    private Bitmap rotateBitmap = null;
    private CategoriesSpinnerAdapter categoriesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Languages.loadLocaleLang(SendPictureActivity.this);

        // UNIVERSAL IMAGE LOADER SETUP
        defaultOptions = new DisplayImageOptions.Builder()
                /*.showImageOnLoading(R.drawable.Reclamationvide)
                .showImageForEmptyUri(R.drawable.Reclamationvide)
				.showImageOnFail(R.drawable.Reclamationvide)*/.cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true).displayer(new CircleBitmapDisplayer(Color.WHITE, 0, 0))
                .resetViewBeforeLoading(true).build();

        config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(defaultOptions).build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        // END - UNIVERSAL IMAGE LOADER SETUP

        ChoixFragment.isFirstStart = false;


        if (savedInstanceState == null) {
            reclamation = new Reclamation();
            setContentView(R.layout.fragment_send_pic);
            //setTitle(getResources().getString(R.string.title_fragment_send_pic));
            progressBar = (ProgressBar) findViewById(R.id.progressBar2);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            String path = (String) getIntent().getExtras().get("image");
            if (path != null) {
                takenPictureFile = new File(path);
            }
            // declaration view's
            resultImageView = (ImageView) findViewById(R.id.takenPic);
            categorieSpinner = (Spinner) findViewById(R.id.categorieSpinner);
            problemtypespinner = (Spinner) findViewById(R.id.problemtypespinner);

            String[] degree = getResources().getStringArray(R.array.degree_array);

            ArrayList<String> myDegreeAdapter = new ArrayList<>();

            //------
            for (int i = 0; i < degree.length; i++) {
                myDegreeAdapter.add(degree[i]);
            }
            //******

            DegreAdapter degreAdapter = new DegreAdapter(this, R.layout.spinner_item, myDegreeAdapter);

            categorieSpinner.setAdapter(degreAdapter);

            categoriesAdapter = new CategoriesSpinnerAdapter(this, R.layout.spinner_item, Category.getCategoriesList(this));

            int index = categoriesAdapter.getIndexOf(cat);
            problemtypespinner.setAdapter(categoriesAdapter);
            problemtypespinner.setSelection(index);
            problemtypespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    cat = categoriesAdapter.getItem(i).get_id();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            comment = (EditText) findViewById(R.id.comment);
            sendPictureBtn = (Button) findViewById(R.id.sendPictureBtn);

            if (Languages.getLocalLanguage(this).equals(Const.FR)) {
                LhachTypeFace.setOoredoo(comment, 0, LhachTypeStyle.REGULAR);
                LhachTypeFace.setOoredoo(sendPictureBtn, 0, LhachTypeStyle.REGULAR);
            } else {
                LhachTypeFace.setOoredooAr(comment, 0, LhachTypeStyle.REGULAR);
                LhachTypeFace.setOoredooAr(sendPictureBtn, 0, LhachTypeStyle.REGULAR);
            }


            // set the send btn listen the click event
            sendPictureBtn.setOnClickListener(this);

            Bitmap bitmap = null;

            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                bitmap = BitmapFactory
                        .decodeFile(takenPictureFile.getAbsolutePath(), options);
                ExifInterface ei = null;

                ei = new ExifInterface(takenPictureFile.getAbsolutePath());

                int orientation = ei.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);
                //
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotateImage(bitmap, 90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotateImage(bitmap, 180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotateImage(bitmap, 270);
                        break;

                    default:
                        int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                        Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                        RoundedDrawableStroke rounddrawable = new RoundedDrawableStroke(scaled, 20, 1);

                        resultImageView.setImageDrawable(rounddrawable);
                        break;
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (UnsatisfiedLinkError e) {
                e.printStackTrace();
            }

            if (RECLAMATION_TYPE.equals(NOTIFY)) {
                categorieSpinner.setVisibility(View.VISIBLE);
            } else {
                categorieSpinner.setVisibility(View.INVISIBLE);
            }
//            List<String> list = new ArrayList<String>();
//            List<Category> categories = Category.getCategoriesList(SendPictureActivity.this);

//            if (Languages.getLocalLanguage(SendPictureActivity.this).equals(Const.FR))
//                for (int i = 0; i < categories.size(); i++) {
//                    categoriesWillBeDisplay.put(categories.get(i).get_categoryFrTitle(), categories.get(i));
//                    list.add(categories.get(i).get_categoryFrTitle());
//                }
//            else
//                for (int i = 0; i < categories.size(); i++) {
//                    categoriesWillBeDisplay.put(categories.get(i).get_categoryArTitle(), categories.get(i));
//                    list.add(categories.get(i).get_categoryArTitle());
//                }

//            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(SendPictureActivity.this,
//                    android.R.layout.simple_spinner_item, list);

            //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

//            categorieSpinner.setPrompt(getResources().getString(R.string.send_pic_categorie_hint));
//            categorieSpinner.setAdapter(dataAdapter);

            rotateBitmap = null;
        }
        System.gc();

    }

    /**
     * Rotate image.
     *
     * @param angle the angle
     * @return the bitmap
     */
    private void rotateImage(final Bitmap bitmap, final float angle) {
        Bitmap mutableBitmap;

        mutableBitmap = BitmapFactory
                .decodeFile(takenPictureFile.getAbsolutePath());

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        try {
            rotateBitmap = Bitmap.createBitmap(mutableBitmap, 0, 0, mutableBitmap.getWidth(),
                    mutableBitmap.getHeight(), matrix, true);
            if (rotateBitmap != null) {
                int nh = (int) (rotateBitmap.getHeight() * (512.0 / rotateBitmap.getWidth()));
                Bitmap scaled = Bitmap.createScaledBitmap(rotateBitmap, 512, nh, true);
                RoundedDrawableStroke rounddrawable = new RoundedDrawableStroke(scaled, 20, 1);

                resultImageView.setImageDrawable(rounddrawable);
                resultImageView.setImageBitmap(scaled);
            } else {
                int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                RoundedDrawableStroke rounddrawable = new RoundedDrawableStroke(scaled, 20, 1);

                resultImageView.setImageDrawable(rounddrawable);
                resultImageView.setImageBitmap(scaled);
            }

        } catch (OutOfMemoryError err) {
            err.printStackTrace();
        }


    }

    @Override
    public void onClick(View v) {

        new UserRepository(this).getActiveUser().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<User>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(User activeUser) {
                        reclamation.setUserId(activeUser.getId());

                        reclamation.setCategoryId(categorieSpinner.getSelectedItem().toString());
                        reclamation.setReclamationType(SendPictureActivity.cat);
                        if (location != null) {
                            reclamation.setLatitude(String.valueOf(location.getLatitude()));
                            reclamation.setLongitude(String.valueOf(location.getLongitude()));
                        } else {
                            reclamation.setLatitude("0");
                            reclamation.setLongitude("0");
                        }
                        reclamation.setComment(comment.getText().toString());

                        reclamation.setPictureFullPath(takenPictureFile.getAbsolutePath());

                        String result = "";
                        result = "userMail:" + activeUser.getEmail() + "_" + "type:" + reclamation.getReclamationType()
                                + "lng:" + reclamation.getLongitude() + "lat:" + reclamation.getLatitude() +
                                "danger_degree :" + reclamation.getCategoryId() + "comment:" + reclamation.getComment();

                        // new ShareSN(this).share(result, "HelpMyCity");

                        new UploadPicTask().execute(reclamation, null, null);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }

    class UploadPicTask extends AsyncTask<Reclamation, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(ProgressBar.VISIBLE);
            sendPictureBtn.setVisibility(View.GONE);
            sendPictureBtn.setEnabled(false);
        }

        @Override
        protected String doInBackground(Reclamation... params) {
            AppDatabase db = AppDatabase.getDatabase(SendPictureActivity.this);
            String accessToken = db.userDao().getActiveUser().getAccessToken();
            String result = new Communication().sampleUploadImage(params[0], accessToken);

            return result;
        }

        @Override
        protected void onPostExecute(String resul) {
            super.onPostExecute(resul);
            ApiResponse apiResponse = new Gson().fromJson(resul, ApiResponse.class);
            try {
                if (apiResponse.isSuccess()) {
                    Toast.makeText(SendPictureActivity.this, getResources().getString(R.string.send_pic_alert_msg), Toast.LENGTH_SHORT).show();
                    reclamation.setSync("1");
                    saveReclamationAndClose();
                } else {
                    Toast.makeText(SendPictureActivity.this, getResources().getString(R.string.send_subsequently), Toast.LENGTH_SHORT).show();
                    reclamation.setSync("0");
                    saveReclamationAndClose();
                }


                progressBar.setVisibility(View.GONE);
                sendPictureBtn.setVisibility(View.VISIBLE);
                sendPictureBtn.setEnabled(true);

            } catch (Exception e) {
                new LhachToast(SendPictureActivity.this, getResources().getString(R.string.send_subsequently));
                progressBar.setVisibility(View.GONE);
                sendPictureBtn.setVisibility(View.VISIBLE);
                sendPictureBtn.setEnabled(true);
                e.printStackTrace();
                reclamation.setSync("0");
                saveReclamationAndClose();
            }
        }
    }

    private void saveReclamationAndClose() {

        new ReclamationRepository(this).insert(reclamation).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Long aLong) {
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        finish();
                    }

                    @Override
                    public void onComplete() {
                        finish();
                    }
                });

    }


    // The localization functions

    @Override
    public void onActivityUpdated(DetectedActivity detectedActivity) {
        //showActivity(detectedActivity);
    }

    @Override
    public void onGeofenceTransition(Geofence geofence, int transitionType) {
        //showGeofence(geofence, transitionType);
    }

    @Override
    public void onLocationUpdated(Location location) {
        this.location = location;
    }

    private void startLocation() {
        isCapturingLocation = true;
        SmartLocation smartLocation = new SmartLocation.Builder(SendPictureActivity.this).logging(true).build();

        smartLocation.location().start(this);
        smartLocation.activityRecognition().start(this);

        // Create some geofences
        GeofenceModel mestalla = new GeofenceModel.Builder("1").setTransition(Geofence.GEOFENCE_TRANSITION_ENTER).setLatitude(39.47453120000001).setLongitude(-0.358065799999963).setRadius(500).build();
        smartLocation.geofencing().add(mestalla).start(this);
    }

    private void stopLocation() {
        isCapturingLocation = false;

        SmartLocation.with(SendPictureActivity.this).location().stop();
        //locationText.setText("Location stopped!");

        SmartLocation.with(SendPictureActivity.this).activityRecognition().stop();
        //activityText.setText("Activity Recognition stopped!");

        SmartLocation.with(SendPictureActivity.this).geofencing().stop();
        //geofenceText.setText("Geofencing stopped!");
    }

    @Override
    public void onResume() {
        super.onResume();
        startLocation();

    }

    @Override
    public void onPause() {
        super.onPause();
        if (isCapturingLocation) {
            stopLocation();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}