package com.mobapp.mouwatensalah;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.mobapp.mouwatensalah.adapter.ItemCallback;
import com.mobapp.mouwatensalah.adapter.MyAdapter;
import com.mobapp.mouwatensalah.database.repository.UserRepository;
import com.mobapp.mouwatensalah.fragments.ActualityFragment;
import com.mobapp.mouwatensalah.fragments.ChoixFragment;
import com.mobapp.mouwatensalah.fragments.EventFragment;
import com.mobapp.mouwatensalah.fragments.FragmentSignaler;
import com.mobapp.mouwatensalah.tools.CloseAppAlert;
import com.mobapp.mouwatensalah.tools.DownloadAsync;
import com.mobapp.mouwatensalah.tools.Languages;

import java.io.File;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity implements ItemCallback {

    //Similarly we Create a String Resource for the name and email in the header view
    //And we also create a int resource for profile picture in the header view

    private String NAME = "";
    private String EMAIL = "";
    private int PROFILE = R.drawable.ic_drawer;

    private Toolbar toolbar;                              // Declaring the Toolbar Object

    private RecyclerView mRecyclerView;                           // Declaring RecyclerView
    private RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    private RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    private DrawerLayout Drawer;                                  // Declaring DrawerLayout

    private ActionBarDrawerToggle mDrawerToggle;                  // Declaring Action Bar Drawer Toggle

    // declaration the fragment manager to be replaced the container frame with the selected fragment from the left Menu
    private FragmentManager fragmentManager;
    private FragmentSignaler fragmentSignaler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new DownloadAsync(this).execute();

        fragmentSignaler = new FragmentSignaler();

    /* Assinging the toolbar object ot the view
    and setting the the Action bar to our toolbar
     */
        //toolbar = new Toolbar(this);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        //init the FragmentManager
        fragmentManager = getSupportFragmentManager();

        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View

        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size

        mAdapter = new MyAdapter(com.mobapp.mouwatensalah.model.MenuItem.getMenuList(this), NAME, EMAIL, PROFILE, this);       // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        // And passing the titles,icons,header view name, header view email,
        // and header view profile picture

        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager

        Drawer = (DrawerLayout) findViewById(R.id.drawer_layout);        // Drawer object Assigned to the view
        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }


        }; // Drawer Toggle Object Made
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State
        mDrawerToggle.setHomeAsUpIndicator(R.drawable.ic_drawer);
        // update the main content by replacing fragments
        updateFragment(fragmentSignaler);
        setTitle(getResources().getString(R.string.title_fragment_signaler));

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }

    private void updateFragment(Fragment fragment) {
        if (!isFinishing())
            fragmentManager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .commitAllowingStateLoss();
    }

    public void changeFragment() {
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragmentSignaler)
                .commitAllowingStateLoss();
    }


    @Override
    public void onItemSelected(int position) {
        if (Drawer.isDrawerOpen(mRecyclerView)) {
            ((MyAdapter) mAdapter).getItemAtPosition(((MyAdapter) mAdapter).getLastSelectedPosition()).setSelected(false);
            ((MyAdapter) mAdapter).getItemAtPosition(position).setSelected(true);
            //Toast.makeText(getApplicationContext(), "im open", Toast.LENGTH_SHORT).show();
            switch (position - 1) {

                case 0:
                    // update the main content by replacing fragments
                    SendPictureActivity.RECLAMATION_TYPE = SendPictureActivity.NOTIFY;

                    updateFragment(fragmentSignaler);
                    setTitle(getResources().getString(R.string.title_fragment_signaler));
                    break;

                case 1:

                    Intent mIntent = new Intent(this, DisplayReclamationActivity.class);
                    startActivity(mIntent);
                    setTitle(getResources().getString(R.string.title_activity_display_reclamation));

                    break;

                case 2:

                    new UserRepository(this).deleteActiveUser().subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Observer<Integer>() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onNext(Integer integer) {
                                    Intent mIntent1 = new Intent(MainActivity.this, LoginActivity.class);
                                    startActivity(mIntent1);
                                    MyAdapter.lastSelectedPosition = 0;
                                    finish();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    e.printStackTrace();
                                }

                                @Override
                                public void onComplete() {

                                }
                            });

                    break;
            }
            Drawer.closeDrawers();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Languages.loadLocaleLang(this);


    }

    public void startSendPictureActivity(File image) {


        Intent mIntent = new Intent(this, SendPictureActivity.class);
        mIntent.putExtra("image", image.getAbsolutePath());

        startActivity(mIntent);
    }

    public void updateViewWithChoiceFragment(File image) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        Bundle mBundle = new Bundle();
        mBundle.putString("image", image.getAbsolutePath());
        ChoixFragment mFragment = ChoixFragment.newInstance();
        mFragment.setArguments(mBundle);
        updateFragment(mFragment);
    }

    public void updateMenuText() {
        ((MyAdapter) mAdapter).updateItems();
    }

    @Override
    public void onBackPressed() {
        try {
            if (fragmentManager.findFragmentByTag("1").isVisible()) {
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragmentSignaler)
                        .commit();
            } else {
                CloseAppAlert.askUser(this);
            }
        } catch (Exception e) {
            CloseAppAlert.askUser(this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (fragmentSignaler != null)
            fragmentSignaler.onActivityResult(requestCode, resultCode, data);
    }
}