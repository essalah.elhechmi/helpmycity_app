package com.mobapp.mouwatensalah;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mobapp.mouwatensalah.database.repository.UserRepository;
import com.mobapp.mouwatensalah.model.SignupResponse;
import com.mobapp.mouwatensalah.model.User;
import com.mobapp.mouwatensalah.tools.Communication;
import com.mobapp.mouwatensalah.tools.Const;
import com.mobapp.mouwatensalah.tools.HttpMethod;
import com.mobapp.mouwatensalah.tools.Languages;
import com.mobapp.mouwatensalah.tools.LhachTypeFace;
import com.mobapp.mouwatensalah.tools.LhachTypeStyle;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class InscriptionActivity extends AppCompatActivity {

    private static final String TAG = "InscriptionActivity";

    private Toolbar toolbar;                              // Declaring the Toolbar Object

    // declaration editText view's
    private EditText name, lastName, userName, email, password, spassword;

    // declaration of submet button

//    private TextView lbl1;

    private Button submet;

    private ProgressBar progressBar;

    private boolean nameError = false, lastnameError = false, usernameError = false, emailError = false, passwordError = false, spasswordError = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        // set the custom actionbar
//        toolbar = (Toolbar) findViewById(R.id.tool_bar);
//        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.mipmap.ic_back));

        // initialize each view
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //lbl1 = (TextView) findViewById(R.id.lbl1);
        name = (EditText) findViewById(R.id.inscrip_name);
        lastName = (EditText) findViewById(R.id.inscrip_lastname);
        userName = (EditText) findViewById(R.id.inscrip_username);
        email = (EditText) findViewById(R.id.inscrip_email);
        password = (EditText) findViewById(R.id.inscrip_password);
        spassword = (EditText) findViewById(R.id.inscrip_spassword);

        submet = (Button) findViewById(R.id.btn_submet);

        if (Languages.getLocalLanguage(this).equals(Const.FR)) {
            //LhachTypeFace.setOoredoo(lbl1, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(name, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(lastName, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(userName, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(email, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(password, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(spassword, 0, LhachTypeStyle.REGULAR);
        } else {
            //LhachTypeFace.setOoredooAr(lbl1, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(name, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(lastName, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(userName, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(email, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(password, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(spassword, 0, LhachTypeStyle.REGULAR);
        }

        // define the all check possible for each edittext
        initCheckForm();

        submet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isExistEmptyfield = false;

                // check if the user don't write all needed fields
                if (!(name.getText().length() > 0))
                    isExistEmptyfield = true;
                if (!(lastName.getText().length() > 0))
                    isExistEmptyfield = true;
                if (!(userName.getText().length() > 0))
                    isExistEmptyfield = true;
                if (!(email.getText().length() > 0))
                    isExistEmptyfield = true;
                if (!(password.getText().length() > 0))
                    isExistEmptyfield = true;
                if (!(spassword.getText().length() > 0))
                    isExistEmptyfield = true;

                if (isExistEmptyfield) {
                    Toast.makeText(InscriptionActivity.this, getResources().getString(R.string.required_fields), Toast.LENGTH_SHORT).show();
                } else if (nameError && lastnameError && usernameError && emailError && passwordError && spasswordError) {
                    setInscription();
                } else {
                    Toast.makeText(InscriptionActivity.this, getResources().getString(R.string.correct_errors), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setInscription() {
        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        // collect all fields to sent it to the server for inscription
        final HashMap<String, String> params = new HashMap<>();
        params.put(Communication.FIRST_NAME, name.getText().toString());
        params.put(Communication.USER_NAME, userName.getText().toString());
        params.put(Communication.LAST_NAME, lastName.getText().toString());
        params.put(Communication.EMAIL, email.getText().toString());
        params.put(Communication.PASSWORD, password.getText().toString());
        params.put(Communication.PASSWORD_COMFIRMATION, password.getText().toString());
        params.put(Communication.ACTION, "register");
        params.put(Communication.DEVICE_ID, android_id);
        params.put(Communication.LANGUAGE, Languages.getLocalLanguage(this));

        User mUser = new User();
        mUser.setEmail(email.getText().toString());
        mUser.setId(1);
        //mUser.setUserId("1");
        mUser.setName(name.getText().toString());
        mUser.setId(1);
        //mUser.save();

        new UserRepository(this).insert(mUser).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Long id) {
                        Log.d(TAG, "inscription : " + id);
                        new InscriptionTask().execute(params, null, null);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }

    /**
     * method is used for checking valid email id format.
     *
     * @param email
     * @return boolean true for valid false for invalid
     */
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    private void initCheckForm() {
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (email.getText().toString() != null) {
                    if (!isEmailValid(email.getText().toString())) {
                        email.setError(getResources().getString(R.string.invalid_address_email));
                        emailError = false;
                    } else {
                        emailError = true;
                        email.setError(null);
                    }
                } else
                    email.setError(getResources().getString(R.string.invalid_address_email));
            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (password.getText().toString() != null) {
                    if (!(password.getText().toString().length() > 4)) {
                        password.setError(getResources().getString(R.string.invalid_password));
                        passwordError = false;
                    } else {
                        passwordError = true;
                        password.setError(null);
                    }
                } else {
                    password.setError(getResources().getString(R.string.invalid_password));
                }
            }
        });

        spassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (spassword.getText().toString() != null) {
                    if (!(spassword.getText().toString().equals(password.getText().toString()))) {
                        spassword.setError(getResources().getString(R.string.passwords_not_match));
                        spasswordError = false;
                    } else {
                        spasswordError = true;
                        spassword.setError(null);
                    }
                } else {
                    spassword.setError(getResources().getString(R.string.passwords_not_match));
                }
            }
        });

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (name.getText().toString() != null) {
                    if (!(name.getText().toString().length() > 2)) {
                        name.setError(getResources().getString(R.string.min_character));
                        nameError = false;
                    } else {
                        nameError = true;
                        name.setError(null);
                    }
                } else {
                    name.setError(getResources().getString(R.string.min_character));
                }
            }
        });

        lastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (lastName.getText().toString() != null) {
                    if (!(lastName.getText().toString().length() > 2)) {
                        lastName.setError(getResources().getString(R.string.min_character));
                        lastnameError = false;
                    } else {
                        lastnameError = true;
                        lastName.setError(null);
                    }
                } else {
                    lastName.setError(getResources().getString(R.string.min_character));
                }
            }
        });

        userName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (userName.getText().toString() != null) {
                    if (!(userName.getText().toString().length() > 2)) {
                        userName.setError(getResources().getString(R.string.min_character));
                        usernameError = false;
                    } else {
                        usernameError = true;
                        userName.setError(null);

                    }
                } else {
                    userName.setError(getResources().getString(R.string.min_character));
                }
            }
        });
    }

    class InscriptionTask extends AsyncTask<HashMap<String, String>, Void, SignupResponse> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected SignupResponse doInBackground(HashMap<String, String>... params) {

            String result = new Communication().getResponseFromUrl(Communication.URL_INSCRIPTION, params[0], HttpMethod.POST);


            if (result != null)
                return new Gson().fromJson(result, SignupResponse.class);
            else
                return null;
        }

        @Override
        protected void onPostExecute(SignupResponse signupResponse) {
            super.onPostExecute(signupResponse);
            progressBar.setVisibility(View.GONE);
            if (signupResponse != null)
                try {
                    if (signupResponse.isSuccess())
                        startActivity(new Intent(InscriptionActivity.this, MainActivity.class));
                    else
                        Toast.makeText(InscriptionActivity.this, signupResponse.getMessage()/*getResources().getString(R.string.informed_email)*/, Toast.LENGTH_SHORT).show();
                    //startActivity(new Intent(InscriptionActivity.this, MainActivity.class));


                } catch (Exception e) {
                    e.printStackTrace();
                }


        }
    }
}
