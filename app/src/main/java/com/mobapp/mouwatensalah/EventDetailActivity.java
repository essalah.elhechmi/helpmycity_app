package com.mobapp.mouwatensalah;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.mobapp.mouwatensalah.model.Event;


public class EventDetailActivity extends AppCompatActivity {

    public static final String SHARE_KEY = "event_object";

    private WebView detailWebView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        detailWebView = (WebView) findViewById(R.id.webview);
        Event mEvent = getIntent().getExtras().getParcelable(SHARE_KEY);
        if(mEvent != null){
            String htmlData = mEvent.get_description();
            // htmlData = htmlData.replace("font-size:12px;", "font-size:14px;");

            // webView settings
            WebSettings webViewSettings = detailWebView.getSettings();
            webViewSettings.setJavaScriptCanOpenWindowsAutomatically(true);
            webViewSettings.setJavaScriptEnabled(true);
            webViewSettings.setBuiltInZoomControls(false);
            webViewSettings.setPluginState(WebSettings.PluginState.ON);
            detailWebView.loadDataWithBaseURL("x-data://base", htmlData, "text/html",
                    "UTF-8", null);
        }
    }
}
