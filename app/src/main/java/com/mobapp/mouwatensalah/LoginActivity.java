package com.mobapp.mouwatensalah;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.DetectedActivity;
import com.google.gson.Gson;
import com.mobapp.mouwatensalah.database.AppDatabase;
import com.mobapp.mouwatensalah.database.repository.UserRepository;
import com.mobapp.mouwatensalah.model.ApiResponse;
import com.mobapp.mouwatensalah.model.User;
import com.mobapp.mouwatensalah.tools.CloseAppAlert;
import com.mobapp.mouwatensalah.tools.Communication;
import com.mobapp.mouwatensalah.tools.Const;
import com.mobapp.mouwatensalah.tools.DownloadAsync;
import com.mobapp.mouwatensalah.tools.HttpMethod;
import com.mobapp.mouwatensalah.tools.Languages;
import com.mobapp.mouwatensalah.tools.LhachTypeFace;
import com.mobapp.mouwatensalah.tools.LhachTypeStyle;
import com.mobapp.mouwatensalah.tools.SynchronizeData;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.nlopez.smartlocation.OnActivityUpdatedListener;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class LoginActivity extends AppCompatActivity implements OnLocationUpdatedListener, OnActivityUpdatedListener {

    private UserRepository userRepository;

    // define the login button
    public static boolean session = false;

    private boolean isCapturingLocation = false;

    public static User mUser;

    public static ContentResolver contentResolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        contentResolver = getContentResolver();
        userRepository = new UserRepository(this);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new LoginFragment())
                    .commit();
        }


        userRepository.getActiveUser().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<User>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(User session) {
                        if (session != null) {
                            startHomeActivity();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * start Home Activity fi the user logIn and finish login activity
     */
    private void startHomeActivity() {
        Intent mIntent = new Intent(this, MainActivity.class);
        startActivity(mIntent);
        finish();
    }

    private void startLocation() {
        isCapturingLocation = true;
        SmartLocation smartLocation = new SmartLocation.Builder(this).logging(true).build();

        smartLocation.location().start(this);
        smartLocation.activityRecognition().start(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startLocation();
        Languages.loadLocaleLang(this);
    }


    @Override
    public void onBackPressed() {
        CloseAppAlert.askUser(this);
    }

    @Override
    public void onLocationUpdated(Location location) {

    }

    @Override
    public void onActivityUpdated(DetectedActivity detectedActivity) {

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class LoginFragment extends Fragment {

        // define the login button
        private LinearLayout btnLogin;

        // define the two EditText username and password
        private EditText userName, password;

        // define the two textview forget password and inscription Lbl

        private LinearLayout inscription;

        private TextView forgetPassword, lbl_fcb, lbl_login, lbl_inscription, lbl_or;

        private Toolbar toolbar;                              // Declaring the Toolbar Object
        private boolean emailError = false, passwordError = false;

        // define the default app shared preference
        private SharedPreferences preferences;

        /**
         * The SharedPreferences editor.
         */
        private SharedPreferences.Editor editor;


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_login, container, false);

            //----------------

            // initialize the alert parameter is activated or not
            preferences = getActivity().getSharedPreferences(Const.PREF_FILE_NAME, Context.MODE_PRIVATE);
            editor = preferences.edit();

            try {
                if (preferences.contains(Const.PREF_LANGUAGE)) {
//                editor.putString(Const.PREF_LANGUAGE, "FR");
//                editor.commit();
                    Languages.loadLocaleLang(getActivity());
                }

                if (!preferences.contains(Const.PREF_ALERT)) {
                    editor.putBoolean(Const.PREF_ALERT, true);
                    editor.commit();
                }

                if (preferences.contains(Const.PREF_APP_SETUP)) {
                    new DownloadAsync(getContext()).execute();
                } else {
                    editor.putBoolean(Const.PREF_APP_SETUP, true).commit();
                }

                Languages.changeLocalLanguage(getActivity(), Const.FR);
            } catch (ClassCastException e) {
                e.printStackTrace();
            }

            // initialize the login btn
            lbl_or = (TextView) rootView.findViewById(R.id.lbl_or);
            lbl_fcb = (TextView) rootView.findViewById(R.id.lbl_fcb);
            lbl_login = (TextView) rootView.findViewById(R.id.lbl_login);
            lbl_inscription = (TextView) rootView.findViewById(R.id.lbl_inscription);

            btnLogin = (LinearLayout) rootView.findViewById(R.id.btn_login);
            // init the EditText
            userName = (EditText) rootView.findViewById(R.id.username);
            password = (EditText) rootView.findViewById(R.id.password);

            // init the forgetPassword and inscription lbl
            forgetPassword = (TextView) rootView.findViewById(R.id.forgetpassword);
            inscription = (LinearLayout) rootView.findViewById(R.id.inscription);

            // set the custom TypeFace for each View


            LhachTypeFace.setOoredoo(forgetPassword, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(lbl_inscription, 0, LhachTypeStyle.REGULAR);

            LhachTypeFace.setOoredoo(userName, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(password, 0, LhachTypeStyle.REGULAR);

            LhachTypeFace.setOoredoo(lbl_login, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(lbl_or, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(lbl_fcb, 0, LhachTypeStyle.REGULAR);


            // set the btn's listening for the click event
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isExistEmptyField = false;

                    // check if the user don't write all needed fields
                    if (!(userName.getText().length() > 0))
                        isExistEmptyField = true;
                    if (!(password.getText().length() > 0))
                        isExistEmptyField = true;

                    if (isExistEmptyField) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.required_fields), Toast.LENGTH_SHORT).show();
                    } else if (!emailError || !passwordError) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.correct_errors), Toast.LENGTH_SHORT).show();
                    } else
                        logIn();
                }
            });

            initCheckForm();

            // set the event action for forget password
            inscription.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startInscriptionActivity();
                }
            });

            forgetPassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startFPasswordActivity();
                }
            });


            String msg1 = "", msg2 = "";


            String lbl1 = getResources().getString(R.string.condition_genarale_first);
            String lblondition = getResources().getString(R.string.condition_genarale_seconde);
            String lbl3 = getResources().getString(R.string.condition_genarale_thrd);

            msg1 = lbl1 + " " + lblondition + " " + lbl3;

            Spannable sp1 = new SpannableString(msg1);
            sp1.setSpan(new ForegroundColorSpan(Color.BLUE), lbl1.length(), lbl1.length() + lblondition.length() + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            TextView generalcondition = (TextView) rootView.findViewById(R.id.generalcondition);
            generalcondition.setText(sp1);
            generalcondition.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(Communication.URL_CONDITION_GENERALE));
                    startActivity(i);
                }
            });

            // ---


            return rootView;
        }

        private void logIn() {
            String android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            // collect all fields to sent it to the server for inscription
            HashMap<String, String> params = new HashMap<>();
            params.put(Communication.EMAIL, userName.getText().toString());
            params.put(Communication.ACTION, "login");
            params.put(Communication.DEVICE_ID, android_id);
            params.put(Communication.PASSWORD, password.getText().toString());

            new LogInTask().execute(params, null, null);
        }


        /**
         * start Home Activity fi the user logIn and finish login activity
         */
        private void startHomeActivity() {
            Intent mIntent = new Intent(getActivity(), MainActivity.class);
            startActivity(mIntent);
            getActivity().finish();
        }

        /**
         * start Inscription activity
         */
        private void startInscriptionActivity() {
            Intent mIntent = new Intent(getActivity(), InscriptionActivity.class);
            startActivity(mIntent);
        }

        /**
         * start forget password Activity
         */
        private void startFPasswordActivity() {
            Intent mIntent = new Intent(getActivity(), ForgetPasswordActivity.class);
            startActivity(mIntent);
        }

        /**
         * method is used for checking valid email id format.
         *
         * @param email
         * @return boolean true for valid false for invalid
         */
        public boolean isEmailValid(String email) {
            boolean isValid = false;

            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            CharSequence inputStr = email;

            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(inputStr);
            if (matcher.matches()) {
                isValid = true;
            }
            return isValid;
        }

        private void initCheckForm() {
            userName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (userName.getText().toString() != null) {
                        if (!(userName.getText().length() > 2)) {
                            userName.setError(getResources().getString(R.string.min_character));
                            emailError = false;
                        } else {
                            emailError = true;
                            userName.setError(null);
                        }
                    } else
                        userName.setError(getResources().getString(R.string.invalid_address_email));
                }
            });

            password.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!password.getText().toString().isEmpty()) {
                        if (!(password.getText().toString().length() > 4)) {
                            password.setError(getResources().getString(R.string.invalid_password));
                            passwordError = false;
                        } else {
                            passwordError = true;
                            password.setError(null);
                        }
                    } else {
                        password.setError(getResources().getString(R.string.invalid_password));
                    }
                }
            });
        }


        class LogInTask extends AsyncTask<HashMap<String, String>, Void, ApiResponse> {

            private User activeUser;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected ApiResponse doInBackground(HashMap<String, String>... params) {
                SynchronizeData synchronizeData = new SynchronizeData(getActivity());

                User user = null;
                ApiResponse apiResponse = new ApiResponse();
                if (synchronizeData.isNetworkAvailable()) {

                    String result = new Communication().getResponseFromUrl(Communication.URL_LOGIN, params[0], HttpMethod.POST);

                    try {
                        Gson mGson = new Gson();

                        user = mGson.fromJson(result, User.class);

                    } catch (Exception e) {

                    }

                    if (user.getId() > 0) {
                        apiResponse.setSuccess(true);
                        AppDatabase database = AppDatabase.getDatabase(getContext());
                        database.close();
                        activeUser = user;
                        Long id = database.userDao().insert(activeUser);

                        String TAG = "login";
                        Log.d(TAG, "saved user id = " + id);
                    } else {
                        apiResponse.setSuccess(false);
                    }

                }


                return apiResponse;
            }

            @Override
            protected void onPostExecute(ApiResponse apiResponse) {
                super.onPostExecute(apiResponse);
                Resources resources = getResources();
                if (apiResponse != null) {
                    try {

                        if (apiResponse.isSuccess())
                            startHomeActivity();
                        else
                            Toast.makeText(getActivity(), resources.getString(R.string.login_invalid_username), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity(), resources.getString(R.string.network_not_available), Toast.LENGTH_SHORT).show();
                }


            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


}
