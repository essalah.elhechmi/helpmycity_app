package com.mobapp.mouwatensalah.model;

import android.content.Context;

import com.mobapp.mouwatensalah.tools.Communication;
import com.mobapp.mouwatensalah.tools.Const;
import com.mobapp.mouwatensalah.tools.LocallyFiles;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by salah on 21/04/2015.
 */
public class Category {
    private final String ID = "categoryID";
    private final String CATEGORY_TITLE = "categoryTitle";
    private final String CATEGORY_IMAGE = "categoryImage";
    private final String CATEGORY_DESCRIPTION = "categoryDescription";

    private String _id;
    private String _title;
    private String _image;
    private String _description;

    public Category(JSONObject catObject) {
        set_id(catObject.optString(ID));
        set_title(catObject.optString(CATEGORY_TITLE));
        set_image(catObject.optString(CATEGORY_IMAGE));
        set_description(catObject.optString(CATEGORY_DESCRIPTION));
    }

    public static ArrayList<Category> getCategoriesList(Context context) {
        ArrayList<Category> categories = new ArrayList<>();
        LocallyFiles mFiles = new LocallyFiles(context);
        JSONArray catJsonArray = mFiles.getFileContentArray(Communication.FILE_NAME_CATEGORIES);

        if (catJsonArray != null) {
            for (int i = 0; i < catJsonArray.length(); i++) {
                categories.add(new Category(catJsonArray.optJSONObject(i)));
            }
        }
        return categories;
    }


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_title() {
        return _title;
    }

    public void set_title(String _title) {
        this._title = _title;
    }

    public String get_image() {
        return _image;
    }

    public void set_image(String _image) {
        this._image = _image;
    }

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
    }
}