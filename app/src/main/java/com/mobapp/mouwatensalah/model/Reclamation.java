package com.mobapp.mouwatensalah.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.content.Context;
import android.support.annotation.NonNull;

import com.mobapp.mouwatensalah.tools.Communication;
import com.mobapp.mouwatensalah.tools.LocallyFiles;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by salah on 19/04/2015.
 */

@Entity
public class Reclamation {

    public static final String RECLAMATION_IMAGE_URL = "image_url";
    public static final String RECLAMATION_ADDRESS = "address";
    public static final String RECLAMATION_DATE = "date";
    public static final String RECLAMATION_TYPE = "reclamation_type";
    public static final String RECLAMATION_COMMENTAIRE = "commentaire";

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private long id;

    private long userId;

    private String categoryId;

    private String reclamationType;

    private String pictureFullPath;

    private String comment;

    private String latitude;

    private String longitude;

    private String sync;

    private String address;
    private String date;

    public Reclamation() {
        super();
    }

    public Reclamation(String picFullpath, String cmnt, String lat, String longi) {
        super();
        pictureFullPath = picFullpath;
        comment = cmnt;
        latitude = lat;
        longitude = longi;
    }

    public Reclamation(JSONObject object) {

        if (!object.optString(RECLAMATION_ADDRESS).equals("null"))
            setAddress(object.optString(RECLAMATION_ADDRESS));
        else
            setAddress("");

        if (!object.optString(RECLAMATION_DATE).equals("null"))
            setDate(object.optString(RECLAMATION_DATE));
        else
            setDate("");

        if (!object.optString(RECLAMATION_IMAGE_URL).equals("null"))
            setPictureFullPath(object.optString(RECLAMATION_IMAGE_URL).replace("\\/", "/"));
        else
            setPictureFullPath("");
    }

    /*public static List<Reclamation> getReclamationList(Context mContext) {
        List<Reclamation> myReclamations = new ArrayList<>();
        LocallyFiles mFiles = new LocallyFiles(mContext);
        JSONArray profilJsonObject = mFiles.getFileContentArray(Communication.FILE_NAME_PHOTOS);
        if (profilJsonObject != null)
            for (int i = 0; i < profilJsonObject.length(); i++) {
                Reclamation recTmp = new Reclamation(profilJsonObject.optJSONObject(i));
                myReclamations.add(recTmp);
            }

        return myReclamations;
    }*/

    public String getPictureFullPath() {
        return pictureFullPath;
    }

    public void setPictureFullPath(String pictureFullPath) {
        this.pictureFullPath = pictureFullPath;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getReclamationType() {
        return reclamationType;
    }

    public void setReclamationType(String reclamationType) {
        this.reclamationType = reclamationType;
    }

    /*public static List<Reclamation> getAll() {
        return Select.from(Reclamation.class).fetch();
    }*/

    public void setSync(String sync) {
        this.sync = sync;
    }

    public String getSync() {
        return sync;
    }

    /*public static List<Reclamation> getunSyncReclamations() {
        List<Reclamation> result = Select
                .from(Reclamation.class)
                .where("sync = ?", "0")
                .fetch();

        return result;
    }*/

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @NonNull
    public long getId() {
        return id;
    }

    public void setId(@NonNull long id) {
        this.id = id;
    }
}