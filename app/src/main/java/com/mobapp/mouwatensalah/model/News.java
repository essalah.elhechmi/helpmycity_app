package com.mobapp.mouwatensalah.model;

import android.content.Context;

import com.mobapp.mouwatensalah.tools.Communication;
import com.mobapp.mouwatensalah.tools.LocallyFiles;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

// TODO: Auto-generated Javadoc

/**
 * The Class News.
 */
public class News {

    /**
     * The Constant ID.
     */
    public static final String ID = "id";

    /**
     * The Constant URL_IMAGE.
     */
    public static final String URL_IMAGE = "photo_url";

    /**
     * The Constant DATE.
     */
    public static final String DATE = "date";

    /**
     * The Constant TITLE.
     */
    public static final String TITLE = "title";

    /**
     * The Constant HTML.
     */
    public static final String DESCRIPTION = "description";

    /**
     * The Constant HTML.
     */
    public static final String CONTENT = "html_content";


    /**
     * The id.
     */
    private String id;

    /**
     * The url_ image.
     */
    private String url_Image;

    /**
     * The title.
     */
    private String title;

    /**
     * The date.
     */
    private String date;

    /**
     * The html.
     */
    private String content;

    /**
     * The html.
     */
    private String description;

    /**
     * Instantiates a new news.
     *
     * @param newsObject the news object
     */
    public News(JSONObject newsObject) {
        setId(newsObject.optString(ID));
        setUrl_Image(newsObject.optString(URL_IMAGE));
        setTitle(newsObject.optString(TITLE));
        setDate(newsObject.optString(DATE));
        setContent(newsObject.optString(CONTENT));
        setDescription(newsObject.optString(DESCRIPTION));
    }

    /**
     * Gets the list news.
     *
     * @return the list news
     */
    public static ArrayList<News> getListNews(Context mContext) {
        ArrayList<News> myData = new ArrayList<News>();
        LocallyFiles mLocallyFiles = new LocallyFiles(mContext);
        JSONArray arrayNewsJson = mLocallyFiles.getFileContentArray(Communication.FILE_NAME_NEWS);

        if (arrayNewsJson != null) {

            for (int i = 0; i < arrayNewsJson.length(); i++)

            {
                JSONObject crntNewsObject = arrayNewsJson.optJSONObject(i);
                News mNews = new News(crntNewsObject);

                myData.add(mNews);
            }
        }

        return myData;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl_Image() {
        return url_Image;
    }

    public void setUrl_Image(String url_Image) {
        this.url_Image = url_Image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}