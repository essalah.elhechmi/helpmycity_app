package com.mobapp.mouwatensalah.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.mobapp.mouwatensalah.tools.Communication;
import com.mobapp.mouwatensalah.tools.LocallyFiles;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Comment;

import java.util.ArrayList;

/**
 * Created by ELHECHMI on 15/06/2015.
 */
public class Event implements Parcelable{

    /**
     * The Json Keys to parse events
     */
    private final String ID = "id";
    private final String TITLE = "title";
    private final String DESCRIPTION = "description";
    private final String DATE = "date";

    /**
     * Fields declaration
     */
    private String _id;
    private String _title;
    private String _description;
    private String _date;

    public Event(Parcel parcel) {
        set_id(parcel.readString());
        set_date(parcel.readString());
        set_title(parcel.readString());
        set_description(parcel.readString());
    }

    public Event (String id, String date, String title, String description){
        super();
        set_id(id);
        set_date(date);
        set_title(title);
        set_description(description);
    }

    public Event(JSONObject objectArray){
        super();
        set_id(objectArray.optString(ID));
        set_date(objectArray.optString(DATE));
        set_title(objectArray.optString(TITLE));
        set_description(objectArray.optString(DESCRIPTION));
    }

    public static ArrayList<Event> getEvents(Context mContext){
        ArrayList<Event> mEvents = new ArrayList<>();

        LocallyFiles mFiles = new LocallyFiles(mContext);

        JSONArray myEventsObject = mFiles.getFileContentArray(Communication.FILE_NAME_EVENTS);
        if(myEventsObject != null)
            for (int i = 0; i < myEventsObject.length(); i++){
                JSONObject tmpObject = myEventsObject.optJSONObject(i);
                mEvents.add(new Event(tmpObject));
            }

        return  mEvents;
    }


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_title() {
        return _title;
    }

    public void set_title(String _title) {
        this._title = _title;
    }

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
    }

    public String get_date() {
        return _date;
    }

    public void set_date(String _date) {
        this._date = _date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(get_id());
        parcel.writeString(get_date());
        parcel.writeString(get_title());
        parcel.writeString(get_description());
    }

    public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>()
    {
        @Override
        public Event createFromParcel(Parcel source)
        {
            return new Event(source);
        }

        @Override
        public Event[] newArray(int size)
        {
            return new Event[size];
        }
    };

    public static Parcelable.Creator<Event> getCreator()
    {
        return CREATOR;
    }
}