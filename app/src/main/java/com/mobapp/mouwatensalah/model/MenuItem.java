package com.mobapp.mouwatensalah.model;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.mobapp.mouwatensalah.R;

import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Class MenuItem.
 */
public class MenuItem {

    //First We Declare Titles And Icons For Our Navigation Drawer List View
    //This Icons And Titles Are holded in an Array as you can see

    //private static String TITLES[] = {"Signaler", "Suggérer", "Féliciter", "Actualités", "Profil", "Paramètres", "Aide"};
    private static int ICONS2[] = {R.mipmap.sawer, R.mipmap.sawer, R.mipmap.evenement, R.mipmap.actualites, R.mipmap.mesphotos, R.mipmap.sedeconnecter};
    private static int ICONS[] = {R.mipmap.sawer_, R.mipmap.sawer_, R.mipmap.evenements_, R.mipmap.actualites_, R.mipmap.mesphotos_, R.mipmap.sedeconnecter_};

    // position of the item into the menu list
    /**
     * The pos.
     */
    private int pos;
    // icon of the item
    /** The drawable. */
    //private Drawable drawable;

    /** The drawable. */
    //private Drawable selected_drawable;
    // item name to display
    /**
     * The title.
     */
    private String title;

    private Drawable drawable, selected_drawable;

    // verified object if this item is the current selected or no
    /**
     * The is selected.
     */
    private boolean isSelected = false;

    /**
     * specify the header position
     */
    private boolean isHeader = false;

    /**
     * Instantiates a new menu item.
     *
     * @param mTitle the m title
     * @param p      the p
     */
    public MenuItem(Drawable mDrawable, Drawable mSelectedDrawable, String mTitle, int p) {
        setDrawable(mDrawable);
        setSelected_drawable(mSelectedDrawable);
        setTitle(mTitle);
        setPos(p);
    }

    /**
     * Gets the menu list.
     *
     * @param mContext the m context
     * @return a list of Menu items
     */
    public static List<MenuItem> getMenuList(Context mContext) {
        List<MenuItem> mItems = new ArrayList<>();
        String[] TITLES_FR = mContext.getResources().getStringArray(R.array.menu_items_names);
        for (int i = 0; i < TITLES_FR.length; i++) {
            MenuItem item = new MenuItem(mContext.getResources().getDrawable(ICONS[i]), mContext.getResources().getDrawable(ICONS2[i]), TITLES_FR[i], i);
            if (i == 0) {
                item.setIsHeader(true);
            }
            if (i == 1) {
                item.setIsHeader(false);
                item.setSelected(true);
            }
            mItems.add(item);
        }

        return mItems;
    }

    /**
     * Gets the drawable.
     *
     * @return the drawable
     */
    public Drawable getDrawable() {
        return drawable;
    }

    /**
     * Sets the drawable.
     *
     * @param drawable the new drawable
     */
    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Checks if is selected.
     *
     * @return true, if is selected
     */
    public boolean isSelected() {
        return isSelected;
    }

    /**
     * Sets the selected.
     *
     * @param isSelected the new selected
     */
    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    /**
     * Gets the pos.
     *
     * @return the pos
     */
    public int getPos() {
        return pos;
    }

    /**
     * Sets the pos.
     *
     * @param pos the new pos
     */
    public void setPos(int pos) {
        this.pos = pos;
    }

    public Drawable getSelected_drawable() {
        return selected_drawable;
    }

    public void setSelected_drawable(Drawable selected_drawable) {
        this.selected_drawable = selected_drawable;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setIsHeader(boolean isHeader) {
        this.isHeader = isHeader;
    }
}