package com.mobapp.mouwatensalah;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.mobapp.mouwatensalah.adapter.ImagesAdapter;
import com.mobapp.mouwatensalah.database.repository.ReclamationRepository;
import com.mobapp.mouwatensalah.model.Reclamation;
import com.mobapp.mouwatensalah.tools.Const;
import com.mobapp.mouwatensalah.tools.ShareSN;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class DisplayReclamationActivity extends AppCompatActivity {

    private GridView photosListView;
    private ImagesAdapter mImagesAdapter;

    /**
     * The default options.
     */
    private DisplayImageOptions defaultOptions;

    /**
     * The config.
     */
    private ImageLoaderConfiguration config;

    private TextView tv;

    /**
     * The image loader.
     */
    private ImageLoader imageLoader;

    // custom action bar
    private Toolbar toolbar;

    private List<Reclamation> reclamations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_reclamation);

//        toolbar = (Toolbar) findViewById(R.id.tool_bar);
//        setSupportActionBar(toolbar);

        setTitle(getResources().getString(R.string.title_activity_display_reclamation));

        tv = (TextView) findViewById(R.id.tv);
        // UNIVERSAL IMAGE LOADER SETUP
        defaultOptions = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .resetViewBeforeLoading(true).build();

        config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(defaultOptions).build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        new ReclamationRepository(this).getAll().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Reclamation>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<Reclamation> recs) {

                        if (recs != null) {
                            reclamations = recs;
                            init();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }

    private void init() {

        if (reclamations.size() < 1) {
            tv.setVisibility(View.VISIBLE);
        }

        mImagesAdapter = new ImagesAdapter(this, R.layout.image_item, reclamations);

        photosListView = (GridView) findViewById(R.id.photosListView);

        photosListView.setVerticalSpacing(20);
        photosListView.setHorizontalSpacing(20);

        photosListView.setAdapter(mImagesAdapter);
        photosListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                imageLoader.loadImage("file:/" + mImagesAdapter.getItem(position).getPictureFullPath(), new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String s, View view) {
                        Log.d("", "");
                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {
                        Log.d("", "");
                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                        String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                                "/" + Const.DIRECTORY_NAME;
                        File dir = new File(file_path);
                        if (!dir.exists())
                            dir.mkdirs();
                        File file = new File(dir, "share.jpg");
                        FileOutputStream fOut;
                        try {
                            fOut = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                            fOut.flush();
                            fOut.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        new ShareSN(DisplayReclamationActivity.this).shareImage(Uri.fromFile(file), "#Route125");
                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {
                        Log.d("", "");
                    }
                });
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
