package com.mobapp.mouwatensalah.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.mobapp.mouwatensalah.model.Reclamation;
import com.mobapp.mouwatensalah.model.User;
import com.mobapp.mouwatensalah.tools.Const;

@Database(entities = {User.class, Reclamation.class}, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract UserDao userDao();

    public abstract ReclamationDao reclamationDao();

    private static AppDatabase INSTANCE;

    public static synchronized AppDatabase getDatabase(final Context context) {

        return Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, Const.DATABASE_NAME)
                // allow queries on the main thread.
                // Don't do this on a real app! See PersistenceBasicSample for an example.
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();

    }

}
