package com.mobapp.mouwatensalah.database.repository;

import android.content.Context;

import com.mobapp.mouwatensalah.database.AppDatabase;
import com.mobapp.mouwatensalah.database.UserDao;
import com.mobapp.mouwatensalah.model.User;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class UserRepository {

    private UserDao userDao;

    public UserRepository(Context context) {

        AppDatabase db = AppDatabase.getDatabase(context);
        userDao = db.userDao();

    }

    public Observable<Long> insert(final User user) {
        return Observable.create(new ObservableOnSubscribe<Long>() {
            @Override
            public void subscribe(ObservableEmitter<Long> emitter) throws Exception {
                try {
                    emitter.onNext(userDao.insert(user));
                    emitter.onComplete();
                } catch (Exception e) {
                    emitter.onError(e);
                }
            }
        });

    }

    public Observable<User> getActiveUser() {
        return Observable.create(new ObservableOnSubscribe<User>() {
            @Override
            public void subscribe(ObservableEmitter<User> emitter) throws Exception {
                try {
                    emitter.onNext(userDao.getActiveUser());
                    emitter.onComplete();
                } catch (Exception e) {
                    emitter.onError(e);
                }
            }
        });
    }

    public Observable<Integer> delete(final User user) {
        return Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {
                try {
                    emitter.onNext(userDao.delete(user));
                    emitter.onComplete();
                } catch (Exception e) {
                    emitter.onError(e);
                }
            }
        });
    }

    public Observable<Integer> deleteActiveUser() {
        return Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {
                try {
                    emitter.onNext(userDao.delete(userDao.getActiveUser()));
                    emitter.onComplete();
                } catch (Exception e) {
                    emitter.onError(e);
                }
            }
        });
    }
}
