package com.mobapp.mouwatensalah.database.repository;

import android.content.Context;

import com.mobapp.mouwatensalah.database.AppDatabase;
import com.mobapp.mouwatensalah.database.ReclamationDao;
import com.mobapp.mouwatensalah.model.Reclamation;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class ReclamationRepository {

    private ReclamationDao reclamationDao;

    public ReclamationRepository(Context context) {

        AppDatabase db = AppDatabase.getDatabase(context);
        reclamationDao = db.reclamationDao();

    }

    public Observable<Long> insert(final Reclamation reclamation) {
        return Observable.create(new ObservableOnSubscribe<Long>() {
            @Override
            public void subscribe(ObservableEmitter<Long> emitter) throws Exception {
                try {
                    emitter.onNext(reclamationDao.insert(reclamation));
                    emitter.onComplete();
                } catch (Exception e) {
                    e.printStackTrace();
                    emitter.onError(e);
                }
            }
        });
    }

    public Observable<List<Reclamation>> getunSyncReclamations() {

        return Observable.create(new ObservableOnSubscribe<List<Reclamation>>() {
            @Override
            public void subscribe(ObservableEmitter<List<Reclamation>> emitter) throws Exception {
                try {
                    emitter.onNext(reclamationDao.getunSyncReclamations());
                    emitter.onComplete();
                } catch (Exception e) {
                    e.printStackTrace();
                    emitter.onError(e);
                }
            }
        });

    }

    public Observable<List<Reclamation>> getAll() {

        return Observable.create(new ObservableOnSubscribe<List<Reclamation>>() {
            @Override
            public void subscribe(ObservableEmitter<List<Reclamation>> emitter) throws Exception {
                try {
                    emitter.onNext(reclamationDao.getAll());
                    emitter.onComplete();
                } catch (Exception e) {
                    e.printStackTrace();
                    emitter.onError(e);
                }
            }
        });

    }

}
