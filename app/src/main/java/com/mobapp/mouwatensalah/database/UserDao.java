package com.mobapp.mouwatensalah.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.mobapp.mouwatensalah.model.User;

@Dao
public interface UserDao {

    @Insert
    Long insert(User user);

    @Query("SELECT * FROM user LIMIT 1")
    User getActiveUser();

    @Delete
    int delete(User... user);
}
