package com.mobapp.mouwatensalah.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.mobapp.mouwatensalah.model.Reclamation;

import java.util.List;

@Dao
public interface ReclamationDao {

    @Insert
    Long insert(Reclamation reclamation);

    @Query("SELECT * from reclamation where userId= :id LIMIT 1")
    List<Reclamation> getReclamationById(long id);

    @Query("SELECT * FROM reclamation")
    List<Reclamation> getAll();

    @Delete
    int delete(Reclamation reclamation);

    @Query("SELECT * from reclamation where sync = '0'")
    List<Reclamation> getunSyncReclamations();

    /*public static List<Reclamation> getunSyncReclamations() {
        List<Reclamation> result = Select
                .from(Reclamation.class)
                .where("sync = ?", "0")
                .fetch();

        return result;
    }*/
}
