package com.mobapp.mouwatensalah.tools;

/**
 * Created by salah on 17/04/2015.
 */
public class Const {

    public static final String DATABASE_NAME = "helpmycity_db";

    /** The Constant PREF_NAME. */
    public static final String DIRECTORY_NAME = "Route125";

    /** The Constant PREF_NAME. */
    public static final String PREF_FILE_NAME = "GlobalPreference";

    /** The Constant PREF_NAME. */
    public static final String PREF_APP_SETUP = "app_setup";

    /** The Constant PREF_ALERT. */
    public static final String PREF_LANGUAGE = "language";

    /** The Constant PREF_ALERT. */
    public static final String PREF_ALERT = "alert";


    /** The Constant for arabic language */
    public static final String AR = "AR";

    /** The Constant for French language */
    public static final String FR = "FR";


    // all json keys

    /** The Constant for categories json */
    public static final String KEY_CATEGORIES_ARRAY = "category";

    /** The Constant SENDER_ID_KEY. */
    public static final String SENDER_ID_KEY = "SENDER_ID";

    /** The Constant SENDER_ID_VALUE. */
    public static final String SENDER_ID_VALUE = "657080198155";

}
