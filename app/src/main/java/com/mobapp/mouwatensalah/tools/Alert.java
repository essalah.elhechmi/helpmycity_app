package com.mobapp.mouwatensalah.tools;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;

import com.mobapp.mouwatensalah.R;

/**
 * Created by ELHECHMI on 18/06/2015.
 */
public class Alert {


    public static void notify(final AppCompatActivity activity, final String msg) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setTitle(activity.getResources().getString(R.string.info));
        dialog.setMessage(msg);
        dialog.setNegativeButton(activity.getString(R.string.btn_ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub

            }
        });
        dialog.show();
    }
}
