package com.mobapp.mouwatensalah.tools;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by salah on 24/04/2015.
 */
public class CheckAlert {

    // define the default app shared preference
    private static SharedPreferences preferences;

    /**
     * The SharedPreferences editor.
     */
    private static SharedPreferences.Editor editor;


    public static int isAlertActivate(Context activity) {
        // initialize the alert parameter is activated or not
        preferences = activity.getSharedPreferences(Const.PREF_FILE_NAME, Context.MODE_PRIVATE);
        if (preferences.getBoolean(Const.PREF_ALERT, false))
            return 1;
        else
            return 0;
    }
}
