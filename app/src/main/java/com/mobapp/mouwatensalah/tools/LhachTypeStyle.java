package com.mobapp.mouwatensalah.tools;

// TODO: Auto-generated Javadoc
/**
 * The Enum LhachTypeStyle.
 */
public enum LhachTypeStyle {

	/** The bold. */
	BOLD,
	/** The light. */
	LIGHT,
	/** The regular. */
	REGULAR
}