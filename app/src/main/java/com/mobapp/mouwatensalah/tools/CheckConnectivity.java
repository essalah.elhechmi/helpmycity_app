package com.mobapp.mouwatensalah.tools;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.mobapp.mouwatensalah.database.AppDatabase;
import com.mobapp.mouwatensalah.database.repository.ReclamationRepository;
import com.mobapp.mouwatensalah.model.Reclamation;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ELHECHMI on 09/05/2015.
 */
public class CheckConnectivity extends BroadcastReceiver {

    private boolean result = false;
    private Context mContext;
    //private List<Reclamation> syncList;

    @Override
    public void onReceive(Context context, Intent arg1) {

        mContext = context;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isOnline();
            }
        }, 3000);


    }

    private void synchronize(boolean isConnected) {
        if (isConnected) {
            //Toast.makeText(mContext, "Internet Connection Lost", Toast.LENGTH_LONG).show();


            new ReclamationRepository(mContext).getunSyncReclamations().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<List<Reclamation>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(List<Reclamation> syncList) {
                            if (syncList != null) {

                                for (int i = 0; i < syncList.size(); i++) {
                                    new UploadPicTask().execute(syncList.get(i), null, null);
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {

                        }
                    });


        }

    }

    public void isOnline() {
        new AsyncTask<Void, Void, Integer>() {
            @Override
            protected Integer doInBackground(Void... params) {
                try {
                    HttpGet httpGet = new HttpGet("http://www.google.com");
                    HttpParams httpParameters = new BasicHttpParams();
                    // Set the timeout in milliseconds until a connection is established.
                    // The default value is zero, that means the timeout is not used.
                    int timeoutConnection = 3000;
                    HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
                    // Set the default socket timeout (SO_TIMEOUT)
                    // in milliseconds which is the timeout for waiting for data.
                    int timeoutSocket = 5000;
                    HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

                    DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
                    HttpResponse response = httpClient.execute(httpGet);

                    return response.getStatusLine().getStatusCode();
                } catch (Exception e) {
                    return 0;
                }
            }

            @Override
            protected void onPostExecute(Integer aVoid) {
                super.onPostExecute(aVoid);
                if (aVoid == 200)
                    result = true;
                else
                    result = false;

                synchronize(result);
            }
        }.execute();
    }

    class UploadPicTask extends AsyncTask<Reclamation, Void, String> {
        private Reclamation reclamation;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Reclamation... params) {
            reclamation = params[0];
            AppDatabase db = AppDatabase.getDatabase(mContext);
            String accessToken = db.userDao().getActiveUser().getAccessToken();
            return new Communication().sampleUploadImage(reclamation, accessToken);
        }

        @Override
        protected void onPostExecute(String resul) {
            super.onPostExecute(resul);
            JSONObject resultObject = null;
            try {
                resultObject = new JSONObject(resul);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (resultObject != null) {
                Integer resultRequest = Integer.valueOf(resultObject.optString("code"));
                Log.d("image", resultObject.toString());
                if (resultRequest != null)
                    try {
                        switch (resultRequest) {

                            case 1:

                                reclamation.setSync("1");
                                updateReclamation(reclamation);
                                break;

                            case 2:

                                reclamation.setSync("0");
                                updateReclamation(reclamation);
                                break;


                        }
                    } catch (Exception e) {

                    }
            }
        }

        private void updateReclamation(Reclamation reclamation) {
            new ReclamationRepository(mContext).insert(reclamation).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Long>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Long aLong) {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
    }
}