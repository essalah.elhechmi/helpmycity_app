package com.mobapp.mouwatensalah.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;
import android.text.Html;
import android.util.Log;

import com.mobapp.mouwatensalah.BuildConfig;
import com.mobapp.mouwatensalah.database.repository.UserRepository;
import com.mobapp.mouwatensalah.model.Reclamation;
import com.mobapp.mouwatensalah.model.User;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

// TODO: Auto-generated Javadoc

/**
 * Communication class to download data from server.
 *
 * @author ESSALAH ELHECHMI, E-Mail : essalah.elhechmi@gmail.com
 */
public class Communication {

    public static final String BASE_URL_125 = "http://www.plan125.com";

    public static final String FIRST_NAME = "name";
    public static final String LAST_NAME = "lastName";
    public static final String USER_NAME = "username";
    public static final String PASSWORD = "password";
    public static final String PASSWORD_COMFIRMATION = "password";
    public static final String EMAIL = "email";
    public static final String ACTION = "action";
    public static final String DEVICE_ID = "device_id";
    public static final String LANGUAGE = "language";

    public static final int ALL_IS_OK = 1;
    public static final int USER_NAME_ALREADY_EXIST = 2;
    public static final int EMAIL_ALREADY_EXIST = 3;
    public static final int EMAIL_AND_USERNAME_EXIST = 4;

    /**
     * The json.
     */
    private String json = null;

    /**
     * The Constant FILE NAME of news
     */
    public static final String FILE_NAME_EVENTS = "events_file.json";

    /**
     * The Constant FILE NAME of news
     */
    public static final String FILE_NAME_PHOTOS = "photos_file.json";


    /**
     * The Constant FILE NAME of news
     */
    public static final String FILE_NAME_NEWS = "news_file.json";

    /**
     * The Constant FILE NAME of profil
     */
    public static final String FILE_NAME_PROFIL = "profil.json";

    /**
     * The Constant FILE NAME of categories list
     */
    public static final String FILE_NAME_CATEGORIES = "categories.json";

    /**
     * The up load server uri. Inscription
     */
    //public static final String URL_NEWS = "http://www.plan126.com/api/news";
    public static final String URL_NEWS = BuildConfig.BASE_URL + "/api/news";
    /**
     * The up load server uri. Inscription
     */
    public static final String URL_EVENTS = BuildConfig.BASE_URL + "/api/events";

    /**
     * The up load server uri. Inscription
     */
    public static final String URL_PHOTOS = BuildConfig.BASE_URL + "/api/my_photos";


    /**
     * The up load server uri. Inscription
     */
    public static final String URL_INSCRIPTION = BuildConfig.BASE_URL + "/api/auth/signup";

    /**
     * The up load server uri.
     */
    public static final String URL_CATEGORIE = BuildConfig.BASE_URL + "/api/category/categories";

    /**
     * Login uri.
     */
    public static final String URL_LOGIN = BuildConfig.BASE_URL + "/api/auth/signin";

    public static String URL_CONDITION_GENERALE = BASE_URL_125 + "/politique-de-confidentialite";

    /**
     * The up load server uri.
     */
    private String URL_UPLOAD_RECLAMATION = BuildConfig.BASE_URL + "/api/reclamations/add";

    /**
     * The is.
     */
    private InputStream is;

    /**
     * The m context.
     */
    private Context mContext;

    /**
     * The server response code.
     */
    private int serverResponseCode;

    /**
     * Instantiates a new communication.
     */
    public Communication() {
    }

    /**
     * Instantiates a new communication.
     *
     * @param mCtxt the m ctxt
     */
    public Communication(Context mCtxt) {
        mCtxt = mContext;
    }

    /**
     * Method to load Html data from url.
     *
     * @param url the url
     * @return the JSON from url
     */
    public String getJSONFromUrl(String url) {

        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpPost = new HttpGet(url);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
            return null;
        }

        return json;

    }

    /**
     * Method to load Html data from url.
     *
     * @param url    the url
     * @param params map of fields to be sent to the server
     * @return the JSON from url
     */
    public String getResponseFromUrl(String url, HashMap<String, String> params, HttpMethod httpMethod) {
        switch (httpMethod) {
            case GET:
                getRequest(url, params);
                break;
            case POST:
                postRequest(url, params);
                break;
                default:
                    postRequest(url, params);
                    break;
        }

        return json;

    }

    private String postRequest(String url, HashMap<String, String> params) {

        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            // Add your params
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            for (Map.Entry entry : params.entrySet()) {
                nameValuePairs.add(new BasicNameValuePair(entry.getKey().toString(), entry.getValue().toString()));
            }

            UrlEncodedFormEntity parameter = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");

            httpPost.setEntity(parameter);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
            return null;
        }

        return json;

    }

    private String getRequest(String url, HashMap<String, String> params) {

        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);

            // Add your params
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            HttpParams parameter = httpClient.getParams();
            for (Map.Entry entry : params.entrySet()) {
                parameter.setParameter(entry.getKey().toString(), entry.getValue().toString());
                //nameValuePairs.add(new BasicNameValuePair(entry.getKey().toString(), entry.getValue().toString()));
            }

            //UrlEncodedFormEntity parameter = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");

            httpGet.setParams(parameter);

            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
            return null;
        }

        return json;

    }

    /**
     * Resave picture.
     *
     * @param file          the file
     * @param pictureBitmap the picture bitmap
     */
    private void reSavePicture(File file, Bitmap pictureBitmap) {
        OutputStream fOut = null;
        try {
            fOut = new FileOutputStream(file);

            pictureBitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
            // saving
            // the
            // Bitmap
            // to
            // a
            // file
            // compressed
            // as
            // a
            // JPEG
            // with
            // 85%
            // compression
            // rate
            fOut.flush();
            fOut.close(); // do not forget to close the stream

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Rotate image.
     *
     * @param source the source
     * @param angle  the angle
     * @return the bitmap
     */
    private Bitmap rotateImage(Bitmap source, float angle) {

        Bitmap bitmap = null;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        try {
            bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(),
                    source.getHeight(), matrix, true);
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
        }
        return bitmap;
    }

    /**
     * Copy.
     *
     * @param src the src
     * @param dst the dst
     * @param dir the dir
     * @return true, if successful
     */
    public boolean copy(File src, File dst, File dir) {
        try {

            if (!dir.exists()) {
                dir.mkdirs();
            }

            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dst);

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    private static final char PARAMETER_DELIMITER = '&';
    private static final char PARAMETER_EQUALS_CHAR = '=';

    public static String createQueryStringForParameters(Map<String, String> parameters) {
        StringBuilder parametersAsQueryString = new StringBuilder();
        if (parameters != null) {
            boolean firstParameter = true;

            for (String parameterName : parameters.keySet()) {
                if (!firstParameter) {
                    parametersAsQueryString.append(PARAMETER_DELIMITER);
                }

                parametersAsQueryString.append(parameterName)
                        .append(PARAMETER_EQUALS_CHAR)
                        .append(URLEncoder.encode(
                                parameters.get(parameterName)));

                firstParameter = false;
            }
        }
        return parametersAsQueryString.toString();
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }


    /**
     * Upload image.
     *
     * @param reclamation
     * @return the result int
     */
    public String sampleUploadImage(Reclamation reclamation, String accessToken) {

        HttpEntity resEntity;
        String serverResponseCode = "0";
        // get the file from its path
        File sourceFile = new File(reclamation.getPictureFullPath());

        String fileName = reclamation.getPictureFullPath();
        try {
            // copy the file to an other temp folder if it is exist
            if (sourceFile.isFile()) {
                if (copy(sourceFile,

                        new File(Environment.getExternalStorageDirectory()
                                + "/mouwatensalah/mouwatensalah.jpg"),
                        new File(Environment.getExternalStorageDirectory()
                                + "/mouwatensalah"))) {

                    fileName = Environment.getExternalStorageDirectory()
                            + "/mouwatensalah/mouwatensalah.jpg";
                    sourceFile = null;
                    sourceFile = new File(fileName);
                }
            }
        } catch (Exception e) {

        }

        if (!sourceFile.isFile()) {

            // dialog.dismiss();

            Log.e("uploadFile", "Source File not exist :" + fileName);
            return "0";

        } else { // if the file to upload exist, we must correct its orientation
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory
                        .decodeFile(fileName, options);
                ExifInterface ei = new ExifInterface(fileName);
                int orientation = ei.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);
                //
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        bitmap = rotateImage(bitmap, 90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        bitmap = rotateImage(bitmap, 180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        bitmap = rotateImage(bitmap, 270);
                        break;
                }

                // save for new the picture into an other folder to can i modify it
                reSavePicture(sourceFile, bitmap);

            } catch (MalformedURLException ex) {
                ex.printStackTrace();

                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {
                e.printStackTrace();
            }


            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(URL_UPLOAD_RECLAMATION);
                FileBody bin1 = new FileBody(sourceFile);
                MultipartEntity reqEntity = new MultipartEntity();
                post.addHeader("Authorization", "Bearer " + accessToken);

                reqEntity.addPart("photo", bin1);
                //reqEntity.addPart("userMail", new StringBody(user.getEmail(), Charset.forName("UTF-8")));
                reqEntity.addPart("category", new StringBody(reclamation.getReclamationType(), Charset.forName("UTF-8")));
                reqEntity.addPart("longitude", new StringBody(reclamation.getLongitude(), Charset.forName("UTF-8")));
                reqEntity.addPart("latitude", new StringBody(reclamation.getLatitude(), Charset.forName("UTF-8")));
                reqEntity.addPart("danger_degree", new StringBody(reclamation.getCategoryId(), Charset.forName("UTF-8")));
                reqEntity.addPart("description", new StringBody(reclamation.getComment(), Charset.forName("UTF-8")));

                post.setEntity(reqEntity);
                HttpResponse response = client.execute(post);
                resEntity = response.getEntity();
                final String response_str = EntityUtils.toString(resEntity, HTTP.UTF_8);
                serverResponseCode = response_str;
                if (resEntity != null) {
                    sourceFile.delete();
                }
            } catch (Exception ex) {
                Log.e("Debug", "error: " + ex.getMessage(), ex);
            }
            return serverResponseCode;

        }
    }


}