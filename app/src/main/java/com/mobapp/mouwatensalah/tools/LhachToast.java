package com.mobapp.mouwatensalah.tools;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mobapp.mouwatensalah.R;

/**
 * Created by ELHECHMI on 19/06/2015.
 */
public class LhachToast extends Toast {

    @Override
    public void show() {
        super.show();
    }

    public LhachToast(Context context, String message) {
        super(context);
        // Create layout inflator object to inflate toast.xml file
        LayoutInflater inflater = LayoutInflater.from(context);

        // Call toast.xml file for toast layout
        View toastRoot = inflater.inflate(R.layout.toast, null);
        TextView tv = (TextView) toastRoot.findViewById(android.R.id.text1);
        tv.setText(message);
        // Set layout to toast
        setView(toastRoot);
        setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM,
                0, 0);
        setDuration(Toast.LENGTH_SHORT);

        Handler handler = new Handler();
        CountDownTimer countDownTimer = new CountDownTimer(10000, 1000) {
            @Override
            public void onTick(long l) {
                show();
            }

            @Override
            public void onFinish() {
                cancel();
            }
        }.start();
    }
}
