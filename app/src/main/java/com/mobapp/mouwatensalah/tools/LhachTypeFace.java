package com.mobapp.mouwatensalah.tools;

import android.graphics.Typeface;
import android.widget.Button;
import android.widget.TextView;

// TODO: Auto-generated Javadoc
/**
 * TextTypeFace to define TypeFace.
 *
 * @author ESSALAH ELHECHMi , E-MAIL : essalah.elhechmi@gmail.com
 */
public class LhachTypeFace {

    private static final String actionBarTypoName = "SinkinSans-400Regular.otf";
    private static final String typoName = "SinkinSans-400Regular.otf";

	/**
	 * Sets the action bar title type face.
	 *
	 * @param textView the new action bar title type face
	 */
	public static void setActionBarTitleTypeFace(TextView textView) {

		setOoredoo(textView, 20, LhachTypeStyle.REGULAR);
		textView.setTextColor(textView.getResources().getColor(
				android.R.color.white));
		// textView.setTextSize(20);

	}

	/**
     * Sets the ooredoo.
     * if the text size equal to 0 then will be use the declared size into xml file
     * @param textView the text view
     * @param textSize the text size
     * @param typeStyle the type style
     */
    public static void setOoredoo(TextView textView, float textSize,
                                  LhachTypeStyle typeStyle) {

        Typeface typeface = Typeface.createFromAsset(textView.getContext()
                .getAssets(), typoName);
        if (textSize != 0)
            textView.setTextSize(textSize);
        if (typeStyle != null) {
            if (typeStyle == LhachTypeStyle.BOLD)
                typeface = Typeface.createFromAsset(textView.getContext()
                        .getAssets(), typoName);
            else if (typeStyle == LhachTypeStyle.LIGHT)
                typeface = Typeface.createFromAsset(textView.getContext()
                        .getAssets(), typoName);
            textView.setTypeface(typeface);
        } else
            textView.setTypeface(typeface, Typeface.NORMAL);

    }

    /**
     * Sets the ooredoo.
     *
     * @param textView the text view
     * @param textSize the text size
     * @param typeStyle the type style
     */
    public static void setOoredoo(Button textView, float textSize,
                                  LhachTypeStyle typeStyle) {

        Typeface typeface = Typeface.createFromAsset(textView.getContext()
                .getAssets(), typoName);
        if (textSize != 0)
            textView.setTextSize(textSize);
        if (typeStyle != null) {
            if (typeStyle == LhachTypeStyle.BOLD)
                typeface = Typeface.createFromAsset(textView.getContext()
                        .getAssets(), typoName);
            else if (typeStyle == LhachTypeStyle.LIGHT)
                typeface = Typeface.createFromAsset(textView.getContext()
                        .getAssets(), typoName);
            textView.setTypeface(typeface);
        } else
            textView.setTypeface(typeface, Typeface.NORMAL);

    }


    /**
     * Sets the ooredoo.
     *
     * @param textView the text view
     * @param textSize the text size
     * @param typeStyle the type style
     */
    public static void setOoredooAr(TextView textView, float textSize,
                                  LhachTypeStyle typeStyle) {

        Typeface typeface = Typeface.createFromAsset(textView.getContext()
                .getAssets(), typoName);
        if (textSize != 0)
            textView.setTextSize(textSize);
        if (typeStyle != null) {
            if (typeStyle == LhachTypeStyle.BOLD)
                typeface = Typeface.createFromAsset(textView.getContext()
                        .getAssets(), typoName);
            else if (typeStyle == LhachTypeStyle.LIGHT)
                typeface = Typeface.createFromAsset(textView.getContext()
                        .getAssets(), typoName);
            textView.setTypeface(typeface);
        } else
            textView.setTypeface(typeface, Typeface.NORMAL);

    }

    /**
     * Sets the ooredoo.
     *
     * @param textView the text view
     * @param textSize the text size
     * @param typeStyle the type style
     */
    public static void setOoredooAr(Button textView, float textSize,
                                  LhachTypeStyle typeStyle) {

        Typeface typeface = Typeface.createFromAsset(textView.getContext()
                .getAssets(), typoName);
        if (textSize != 0)
            textView.setTextSize(textSize);
        if (typeStyle != null) {
            if (typeStyle == LhachTypeStyle.BOLD)
                typeface = Typeface.createFromAsset(textView.getContext()
                        .getAssets(), typoName);
            else if (typeStyle == LhachTypeStyle.LIGHT)
                typeface = Typeface.createFromAsset(textView.getContext()
                        .getAssets(), typoName);
            textView.setTypeface(typeface);
        } else
            textView.setTypeface(typeface, Typeface.NORMAL);

    }

}
