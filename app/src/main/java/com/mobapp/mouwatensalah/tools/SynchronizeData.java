package com.mobapp.mouwatensalah.tools;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


// TODO: Auto-generated Javadoc

/**
 * The Class SynchronizeData.
 */
public class SynchronizeData {
	
	/** The m context. */
	public Context mContext;

	/**
	 * Instantiates a new synchronize data.
	 *
	 * @param ctx the ctx
	 */
	public SynchronizeData(Context ctx) {
		mContext = ctx;
	}

	/**
	 * The Class JSONRunnable.
	 */
	public class JSONRunnable implements Runnable {
		
		/**
		 * Instantiates a new JSON runnable.
		 *
		 * @param ctx the ctx
		 */
		public JSONRunnable(Context ctx) {
			mContext = ctx;
		}

		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			new Downloads().execute();
		}

	}

	/**
	 * The Class Downloads.
	 */
	class Downloads extends AsyncTask<Void, Void, Void> {

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPreExecute()
		 */
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			Log.d("download_start", "download start");
		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
		@Override
		protected Void doInBackground(Void... params) {

			// download equipes json file
			if (isNetworkAvailable()) {
//				downloadJSON(Communication.URL_SURVEY,
//						Communication.FILE_NAME_SURVEY);

			}

			return null;
		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		}

	}

	/**
	 * Checks if is network available.
	 *
	 * @return true, if is network available
	 */
	public boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) mContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	/**
	 * Checks if is valid json.
	 *
	 * @param jSonString the j son string
	 * @return true, if is valid json
	 */
	public boolean isValidJSON(String jSonString) {
		try {

			if (jSonString != null) {
				@SuppressWarnings("unused")
				JSONArray test = new JSONArray(jSonString);
				return true;
			} else {
				return false;
			}

		} catch (JSONException e) {

			try {
				@SuppressWarnings("unused")
				JSONObject test = new JSONObject(jSonString);
				return true;
			} catch (JSONException e1) {
				return false;
			}
		}
	}

	/**
	 * Download json.
	 *
	 * @param url the url
	 * @param fileName the file name
	 */
	public void downloadJSON(String url, String fileName, HashMap<String, String> params) {
		Communication connectToServer = new Communication();
		LocallyFiles mLocallyFiles = new LocallyFiles(mContext);
		String jSonString = null;
		jSonString = connectToServer.getResponseFromUrl(url, params, HttpMethod.GET);

		if (isValidJSON(jSonString)) {

			//JSONObject dataObject;
			try {

				mLocallyFiles.saveLocallyFile(fileName, jSonString);

				Log.d("Response: " + fileName + " JSON ", "> OK");
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * Download json.
	 *
	 * @param url the url
	 * @param fileName the file name
	 */
	public void downloadJSON(String url, String fileName) {
		Communication connectToServer = new Communication();
		LocallyFiles mLocallyFiles = new LocallyFiles(mContext);
		String jSonString = null;

			jSonString = connectToServer.getJSONFromUrl(url);

		if (isValidJSON(jSonString)) {

			try {

				mLocallyFiles.saveLocallyFile(fileName, jSonString);
				// }
				Log.d("Response: " + fileName + " JSON ", "> OK");
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

}