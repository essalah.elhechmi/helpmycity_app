package com.mobapp.mouwatensalah.tools;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import java.util.Locale;

public class Languages {

    public static String myLocale = Const.FR;
    // define the default app shared preference
    private static SharedPreferences preferences;

    /**
     * The SharedPreferences editor.
     */
    private static SharedPreferences.Editor editor;

    public static void changeLocalLanguage(Context activity, String lng) {
        // initialize the alert parameter is activated or not
        preferences = activity.getSharedPreferences(Const.PREF_FILE_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(Const.PREF_LANGUAGE, lng);
        editor.commit();
    }

    public static String getLocalLanguage(Context activity) {
        // initialize the alert parameter is activated or not
        preferences = activity.getSharedPreferences(Const.PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(Const.PREF_LANGUAGE, Const.FR);
    }

    public static void loadLocaleLang(Activity context) {
        // initialize the alert parameter is activated or not
        preferences = context.getSharedPreferences(Const.PREF_FILE_NAME, Context.MODE_PRIVATE);

        try {
            if (preferences.contains(Const.PREF_LANGUAGE)) {
                myLocale = preferences.getString(Const.PREF_LANGUAGE, Const.FR);
            }
        } catch (ClassCastException e) {
            e.printStackTrace();
        }

        Locale locale = new Locale(Languages.myLocale);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getBaseContext()
                .getResources()
                .updateConfiguration(
                        config,
                        context.getBaseContext().getResources()
                                .getDisplayMetrics());
    }

}