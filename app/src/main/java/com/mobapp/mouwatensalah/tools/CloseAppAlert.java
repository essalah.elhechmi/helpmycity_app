package com.mobapp.mouwatensalah.tools;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatActivity;

import com.mobapp.mouwatensalah.R;

/**
 * Created by salah on 23/04/2015.
 */
public class CloseAppAlert {

    public static void askUser(final AppCompatActivity activity) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setTitle(activity.getResources().getString(R.string.info));
        dialog.setMessage(activity.getResources().getString(R.string.close_app_msg));
        dialog.setPositiveButton(activity.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                activity.finish();
                activity.moveTaskToBack(true);
                System.exit(0);
                //get gps
            }
        });
        dialog.setNegativeButton(activity.getString(R.string.cancel), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub

            }
        });
        dialog.show();
    }
}
