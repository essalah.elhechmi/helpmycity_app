package com.mobapp.mouwatensalah.tools;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

public class ShareSN {

    private Activity act;
    private String textToShare;
    private String subject;
    private Uri uri;

    public ShareSN(Activity mAct, Uri uri,
                   String subject) {
        act = mAct;
        this.subject = subject;
        this.uri = uri;
    }

    public ShareSN(Activity mAct) {
        act = mAct;
    }

    public void shareImage(Uri uri, String subject) {

        Intent share = new Intent(Intent.ACTION_SEND);

        share.setType("image/*");
        share.putExtra(Intent.EXTRA_STREAM, uri);


        act.startActivity(Intent.createChooser(share, "Partager"));

    }

    public void share(String txtToShare, String subject) {

        setSubject(subject);
        setTextToShare(txtToShare);
        startShare();

    }

    private void startShare() {

        // create the send intent
        Intent shareIntent = new Intent(Intent.ACTION_SEND);

        // set the type
        shareIntent.setType("text/plain");
        try {

            shareIntent.putExtra(Intent.EXTRA_TEXT,
                    getTextToShare());

            act.startActivity(Intent.createChooser(shareIntent, "Partager"));
        } catch (Exception e) {
            // TODO Auto-generated catch
            e.printStackTrace();
        }

    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String msubject) {
        subject = msubject;
    }

    public String getTextToShare() {
        return textToShare;
    }

    public void setTextToShare(String textToShare) {
        this.textToShare = textToShare;
    }

}