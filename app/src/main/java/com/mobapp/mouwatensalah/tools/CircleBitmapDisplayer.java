package com.mobapp.mouwatensalah.tools;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;

import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;

// TODO: Auto-generated Javadoc
/**
 * The Class CircleBitmapDisplayer.
 */
public class CircleBitmapDisplayer implements BitmapDisplayer {

	/** The border width. */
	private int borderWidth = 0;
	
	/** The border color. */
	private int borderColor;

	/** The radius size. */
	private int radiusSize;

	/**
	 * Instantiates a new circle bitmap displayer.
	 */
	public CircleBitmapDisplayer() {
		super();
	}

	/**
	 * Instantiates a new circle bitmap displayer.
	 *
	 * @param borderColor the border color
	 * @param borderWidth the border width
	 */
	public CircleBitmapDisplayer(int borderColor, int radius, int borderWidth) {
		super();
		this.borderColor = borderColor;
		this.borderWidth = borderWidth;
		this.radiusSize = radius;
	}

	/* (non-Javadoc)
	 * @see com.nostra13.universalimageloader.core.display.BitmapDisplayer#display(android.graphics.Bitmap, com.nostra13.universalimageloader.core.imageaware.ImageAware, com.nostra13.universalimageloader.core.assist.LoadedFrom)
	 */
	@Override
	public void display(Bitmap bitmap, ImageAware imageAware,
			LoadedFrom loadedFrom) {
		imageAware.setImageDrawable(new RoundedDrawableStroke(bitmap, radiusSize, borderWidth));
	}

	/**
	 * The Class RoundedDrawableStroke.
	 */
	public class RoundedDrawableStroke extends Drawable {

		/** The corner radius. */
		protected final float cornerRadius;
		
		/** The margin. */
		protected final int margin;

		/** The m bitmap rect. */
		protected final RectF mRect = new RectF(), mBitmapRect;
		
		/** The bitmap shader. */
		protected final BitmapShader bitmapShader;
		
		/** The paint. */
		protected final Paint paint;

		/**
		 * Instantiates a new rounded drawable stroke.
		 *
		 * @param bitmap the bitmap
		 * @param cornerRadius the corner radius
		 * @param margin the margin
		 */
		public RoundedDrawableStroke(Bitmap bitmap, int cornerRadius, int margin) {
			this.cornerRadius = cornerRadius;
			this.margin = margin;

			bitmapShader = new BitmapShader(bitmap, Shader.TileMode.CLAMP,
					Shader.TileMode.CLAMP);
			mBitmapRect = new RectF(margin, margin, bitmap.getWidth() - margin,
					bitmap.getHeight() - margin);

			paint = new Paint();
			paint.setAntiAlias(true);
			paint.setShader(bitmapShader);
		}

		/* (non-Javadoc)
		 * @see android.graphics.drawable.Drawable#onBoundsChange(android.graphics.Rect)
		 */
		@Override
		protected void onBoundsChange(Rect bounds) {
			super.onBoundsChange(bounds);
			mRect.set(margin, margin, bounds.width() - margin, bounds.height()
					- margin);

			// Resize the original bitmap to fit the new bound
			Matrix shaderMatrix = new Matrix();
			shaderMatrix.setRectToRect(mBitmapRect, mRect,
					Matrix.ScaleToFit.FILL);
			bitmapShader.setLocalMatrix(shaderMatrix);

		}

		/* (non-Javadoc)
		 * @see android.graphics.drawable.Drawable#draw(android.graphics.Canvas)
		 */
		@Override
		public void draw(Canvas canvas) {
			canvas.drawRoundRect(mRect, cornerRadius, cornerRadius, paint);
			if (borderWidth > 0) {
				final Paint paint2 = new Paint();
				paint2.setAntiAlias(true);
				paint2.setColor(borderColor);
				paint2.setStrokeWidth(borderWidth);
				paint2.setStyle(Paint.Style.STROKE);

				float radius = mRect.width() > mRect.height() ? ((float) mRect
						.height()) / 2f : ((float) mRect.width()) / 2f;
				canvas.drawCircle(mRect.width() / 2, mRect.height() / 2, radius
						- borderWidth / 2, paint2);
			}
		}

		/* (non-Javadoc)
		 * @see android.graphics.drawable.Drawable#getOpacity()
		 */
		@Override
		public int getOpacity() {
			return PixelFormat.TRANSLUCENT;
		}

		/* (non-Javadoc)
		 * @see android.graphics.drawable.Drawable#setAlpha(int)
		 */
		@Override
		public void setAlpha(int alpha) {
			paint.setAlpha(alpha);
		}

		/* (non-Javadoc)
		 * @see android.graphics.drawable.Drawable#setColorFilter(android.graphics.ColorFilter)
		 */
		@Override
		public void setColorFilter(ColorFilter cf) {
			paint.setColorFilter(cf);
		}
	}
}