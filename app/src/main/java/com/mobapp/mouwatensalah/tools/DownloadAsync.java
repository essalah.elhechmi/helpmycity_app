package com.mobapp.mouwatensalah.tools;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;

import com.mobapp.mouwatensalah.database.AppDatabase;
import com.mobapp.mouwatensalah.model.User;

import java.util.HashMap;

/**
 * Created by ELHECHMI on 11/06/2015.
 */
public class DownloadAsync extends AsyncTask<Void, Void, Void> {

    private Context mContext;

    public DownloadAsync(Context context) {
        mContext = context;
    }

    @Override
    protected Void doInBackground(Void... params) {
        final SynchronizeData synchronizeData = new SynchronizeData(mContext);
        synchronizeData.downloadJSON(Communication.URL_CATEGORIE, Communication.FILE_NAME_CATEGORIES);

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}
