package com.mobapp.mouwatensalah;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobapp.mouwatensalah.tools.Communication;
import com.mobapp.mouwatensalah.tools.Const;
import com.mobapp.mouwatensalah.tools.Languages;
import com.mobapp.mouwatensalah.tools.LhachTypeFace;
import com.mobapp.mouwatensalah.tools.LhachTypeStyle;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ForgetPasswordActivity extends AppCompatActivity {

    // declaration an object to verify with it the email validation status
    private boolean emailError;
    // declaration of Email editText
    private EditText email;

    // declaration of send Button
    private Button sendBtn;
    private Toolbar toolbar;
    private TextView lbl1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

//        toolbar = (Toolbar) findViewById(R.id.tool_bar);
//        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.mipmap.ic_back));

        // init the email edittext


        lbl1 = (TextView) findViewById(R.id.lbl1);
        if (Languages.getLocalLanguage(this).equals(Const.FR))
            LhachTypeFace.setOoredoo(lbl1, 0, LhachTypeStyle.REGULAR);
        else
            LhachTypeFace.setOoredooAr(lbl1, 0, LhachTypeStyle.REGULAR);

        email = (EditText) findViewById(R.id.email);

        // init the send button
        sendBtn = (Button) findViewById(R.id.btn_send);

        // set listener the click event for send btn
        if (Languages.getLocalLanguage(this).equals(Const.FR)) {
            LhachTypeFace.setOoredoo(lbl1, 0, LhachTypeStyle.REGULAR);
        } else {
            LhachTypeFace.setOoredooAr(lbl1, 0, LhachTypeStyle.REGULAR);
        }

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(email.getText().length() > 0)) {
                    Toast.makeText(ForgetPasswordActivity.this, getResources().getString(R.string.required_fields), Toast.LENGTH_SHORT).show();
                } else if (emailError) {
                    // parameters will be send to the server
                    HashMap<String, String> paramsHttp = new HashMap<String, String>();
                    paramsHttp.put(Communication.LANGUAGE, Languages.getLocalLanguage(ForgetPasswordActivity.this));
                    paramsHttp.put(Communication.EMAIL, email.getText().toString());
                    new ResetPasswordTask().execute(paramsHttp, null, null);
                } else {
                    Toast.makeText(ForgetPasswordActivity.this, getResources().getString(R.string.correct_errors), Toast.LENGTH_SHORT).show();
                }
            }
        });

        initCheckForm();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * method is used for checking valid email id format.
     *
     * @param email
     * @return boolean true for valid false for invalid
     */
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


    private void initCheckForm() {
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (email.getText().toString() != null) {
                    if (!isEmailValid(email.getText().toString())) {
                        email.setError(getResources().getString(R.string.invalid_address_email));
                        emailError = false;
                    } else {
                        emailError = true;
                        email.setError(null);
                    }
                } else
                    email.setError(getResources().getString(R.string.invalid_address_email));
            }
        });
    }

    class ResetPasswordTask extends AsyncTask<HashMap<String, String>, Void, String> {

        @Override
        protected String doInBackground(HashMap<String, String>... params) {

            return null;/*new Communication().getRequestFromUrl(Communication.URL_FORGET_PASSWORD, params[0]);*/
        }

        @Override
        protected void onPostExecute(String resultRequest) {
            super.onPostExecute(resultRequest);
            Resources resources = getResources();
            if (!resultRequest.equals(null))
                try {
                    int result = Integer.valueOf(resultRequest);
                    switch (result) {
                        case 1:
                            finish();
                            Toast.makeText(ForgetPasswordActivity.this, resources.getString(R.string.fp_alert), Toast.LENGTH_SHORT).show();
                            break;

                        case 2:
                            Toast.makeText(ForgetPasswordActivity.this, resources.getString(R.string.invalid_address_email), Toast.LENGTH_SHORT).show();
                            break;


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


        }
    }
}
