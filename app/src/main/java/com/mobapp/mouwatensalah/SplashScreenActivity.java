package com.mobapp.mouwatensalah;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.mobapp.mouwatensalah.tools.Communication;
import com.mobapp.mouwatensalah.tools.Const;
import com.mobapp.mouwatensalah.tools.Languages;
import com.mobapp.mouwatensalah.tools.LhachTypeFace;
import com.mobapp.mouwatensalah.tools.LhachTypeStyle;
import com.mobapp.mouwatensalah.tools.SynchronizeData;


public class SplashScreenActivity extends AppCompatActivity {

    // declaration of labels
    private TextView lbl1, lbl2, lbl3;

    // define the default app shared preference
    private SharedPreferences preferences;

    /**
     * The SharedPreferences editor.
     */
    private SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // initialize the alert parameter is activated or not
        preferences = getSharedPreferences(Const.PREF_FILE_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();

        try {
            if (preferences.contains(Const.PREF_LANGUAGE)) {
//                editor.putString(Const.PREF_LANGUAGE, "FR");
//                editor.commit();
                Languages.loadLocaleLang(this);
            }
        } catch (ClassCastException e) {
            e.printStackTrace();
        }

        setContentView(R.layout.activity_splash_screen);

        // initialize view's
        lbl1 = (TextView) findViewById(R.id.lbl1);
        lbl2 = (TextView) findViewById(R.id.lbl2);
        lbl3 = (TextView) findViewById(R.id.lbl3);

        // set the custom TypeFace "Ooredoo.ttf"
        if (Languages.getLocalLanguage(this).equals(Const.FR)) {
            LhachTypeFace.setOoredoo(lbl1, 0, LhachTypeStyle.BOLD);
            LhachTypeFace.setOoredoo(lbl2, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredoo(lbl3, 0, LhachTypeStyle.LIGHT);
        } else {
            LhachTypeFace.setOoredooAr(lbl1, 0, LhachTypeStyle.BOLD);
            LhachTypeFace.setOoredooAr(lbl2, 0, LhachTypeStyle.REGULAR);
            LhachTypeFace.setOoredooAr(lbl3, 0, LhachTypeStyle.LIGHT);
        }

        new Update().execute();
    }

    private void startLngActivity() {
        Intent mIntent;
        if (preferences.contains(Const.PREF_LANGUAGE)) {
            mIntent = new Intent(this, LoginActivity.class);
        } else {
            mIntent = new Intent(this, LoginActivity.class);
        }

        if (!preferences.contains(Const.PREF_ALERT)) {
            editor.putBoolean(Const.PREF_ALERT, true);
            editor.commit();
        }

        startActivity(mIntent);
        finish();
    }

    class Update extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            final SynchronizeData synchronizeData = new SynchronizeData(SplashScreenActivity.this);
            synchronizeData.downloadJSON(Communication.URL_CATEGORIE, Communication.FILE_NAME_CATEGORIES);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startLngActivity();
                }
            }, 2000);
        }
    }
}