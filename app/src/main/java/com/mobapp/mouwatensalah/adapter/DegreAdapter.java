package com.mobapp.mouwatensalah.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mobapp.mouwatensalah.model.Category;

import java.util.ArrayList;

/**
 * Created by ELHECHMI on 18/06/2015.
 */
public class DegreAdapter extends ArrayAdapter<String> {

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private ArrayList<String> values;

    /**
     * The m inflater.
     */
    private LayoutInflater mInflater;

    private int res;

    public DegreAdapter(Context context, int textViewResourceId,
                                    ArrayList<String> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
        this.mInflater = LayoutInflater.from(context);
        this.res = textViewResourceId;
    }

    public int getCount() {
        return values.size();
    }

    public String getItem(int position) {
        return values.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label;
        convertView = mInflater.inflate(res, parent, false);
        label = (TextView) convertView.findViewById(android.R.id.text1);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)

        label.setText(values.get(position));

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = new TextView(context);
        label.setText(values.get(position));
        label.setPadding(10, 10, 10, 10);

        return label;
    }

    public int getIndexOf(String txt) {
        int index = 0;
        for (int i = 0; i < values.size(); i++) {
            if (values.get(i).equals(txt)) {
                index = i;
                return index;
            }
        }
        return index;
    }
}