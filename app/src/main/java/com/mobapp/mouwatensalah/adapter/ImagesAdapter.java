package com.mobapp.mouwatensalah.adapter;

import android.content.Context;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobapp.mouwatensalah.R;
import com.mobapp.mouwatensalah.model.Reclamation;
import com.mobapp.mouwatensalah.tools.CircleBitmapDisplayer;
import com.mobapp.mouwatensalah.tools.LhachTypeFace;
import com.mobapp.mouwatensalah.tools.LhachTypeStyle;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;
import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Class ReclamationAdapter.
 */
public class ImagesAdapter extends BaseAdapter {

    /**
     * The m context.
     */
    public Context mContext;

    /**
     * The m items.
     */
    private List<Reclamation> mItems;

    /**
     * The resource.
     */
    private int resource;

    /**
     * The m inflater.
     */
    private LayoutInflater mInflater;

    /**
     * The default options.
     */
    private DisplayImageOptions defaultOptions;

    /**
     * The config.
     */
    private ImageLoaderConfiguration config;

    /**
     * The image loader.
     */
    private ImageLoader imageLoader;

    /**
     * Instantiates a new Reclamation adapter.
     *
     * @param context  the context
     * @param resource the resource
     * @param objects  the objects
     */
    public ImagesAdapter(Context context, int resource, List<Reclamation> objects) {
        init(context, R.layout.image_item, objects);
    }

    /**
     * Inits the.
     *
     * @param context  the context
     * @param resource the resource
     * @param objects  the objects
     */
    public void init(Context context, int resource, List<Reclamation> objects) {
        this.mContext = context;
        this.mItems = objects;
        this.resource = resource;
        this.mInflater = LayoutInflater.from(context);

        // UNIVERSAL IMAGE LOADER SETUP
        // UNIVERSAL IMAGE LOADER SETUP
        defaultOptions = new DisplayImageOptions.Builder()
                /*.showImageOnLoading(R.drawable.Reclamationvide)
				.showImageForEmptyUri(R.drawable.Reclamationvide)
				.showImageOnFail(R.drawable.Reclamationvide)*/.cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true).displayer(new CircleBitmapDisplayer(mContext.getResources().getColor(android.R.color.transparent),10, 0))
                .resetViewBeforeLoading(true).build();

        config = new ImageLoaderConfiguration.Builder(mContext)
                .defaultDisplayImageOptions(defaultOptions).build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        // END - UNIVERSAL IMAGE LOADER SETUP
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
     */
    @SuppressWarnings("deprecation")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Reclamation item = mItems.get(position);
        Holder holder;
        if (convertView == null) {
            holder = new Holder();

            // initialize the view
            convertView = mInflater.inflate(resource, parent, false);
            holder.drawable = (ImageView) convertView.findViewById(R.id.image);
            //holder.date = (ImageButton) convertView.findViewById(R.id.date);

            holder.address = (TextView) convertView.findViewById(R.id.address);
            LhachTypeFace.setOoredoo(holder.address, 0, LhachTypeStyle.BOLD);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        File imgFile = new File(item.getPictureFullPath());

        //holder.drawable.setImageURI(Uri.fromFile(imgFile));
        //imageLoader.displayImage("file://" + item.getPictureFullPath(), holder.drawable, defaultOptions);
        //imageLoader.displayImage(item.getPictureFullPath(), holder.drawable, defaultOptions);
        holder.drawable.setImageURI(Uri.fromFile(new File(item.getPictureFullPath())));

        String address = "", date = "";

            if(item.getAddress() != null)
            address = Html.fromHtml(item.getAddress()).toString();
            if(item.getDate() != null)
            date = item.getDate();

            holder.address.setText(address);

        return convertView;
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getCount()
     */
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mItems.size();
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
    public Reclamation getItem(int arg0) {
        return mItems.get(arg0);
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * The Class Holder.
     */
    class Holder {

        /**
         * The times.
         */
        public TextView address;

        /**
         * The drawable1.
         */
        public ImageView drawable;

        public ImageButton date;
    }

}