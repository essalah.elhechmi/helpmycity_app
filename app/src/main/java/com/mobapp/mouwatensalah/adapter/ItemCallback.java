package com.mobapp.mouwatensalah.adapter;

/**
 * Created by salah on 24/03/2015.
 */
public interface ItemCallback {

    /**
     * Callback . when an item has been selected.
     */
    public void onItemSelected(int position);
}
