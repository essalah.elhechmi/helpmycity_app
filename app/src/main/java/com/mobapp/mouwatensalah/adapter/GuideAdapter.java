package com.mobapp.mouwatensalah.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mobapp.mouwatensalah.R;
import com.mobapp.mouwatensalah.tools.Const;
import com.mobapp.mouwatensalah.tools.Languages;

/**
 * Created by salah on 24/04/2015.
 */
public class GuideAdapter extends PagerAdapter {
    private Context context;
    private int[] GalImagesFr = new int[]{
            R.drawable.one_fr,
            R.drawable.two_fr,
            R.drawable.three_fr,
            R.drawable.four_fr
    };

    private int[] GalImagesAr = new int[]{
            R.drawable.one_ar,
            R.drawable.two_ar,
            R.drawable.three_ar,
            R.drawable.four_ar
    };

    public GuideAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return GalImagesFr.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);
        int padding = 0;
        imageView.setPadding(padding, padding, padding, padding);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        if (Languages.getLocalLanguage(this.context).equals(Const.FR)) {
            imageView.setImageResource(GalImagesFr[position]);
        } else {
            imageView.setImageResource(GalImagesAr[position]);
        }

            ((ViewPager) container).addView(imageView, position-1);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }
}