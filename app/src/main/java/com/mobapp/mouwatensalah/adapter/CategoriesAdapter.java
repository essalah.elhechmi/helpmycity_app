package com.mobapp.mouwatensalah.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.mobapp.mouwatensalah.R;
import com.mobapp.mouwatensalah.model.Category;
import com.mobapp.mouwatensalah.model.News;
import com.mobapp.mouwatensalah.tools.Const;
import com.mobapp.mouwatensalah.tools.Languages;
import com.mobapp.mouwatensalah.tools.LhachTypeFace;
import com.mobapp.mouwatensalah.tools.LhachTypeStyle;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by ELHECHMI on 11/06/2015.
 */
public class CategoriesAdapter extends BaseAdapter {

    /**
     * The m context.
     */
    public Context mContext;

    /**
     * The m items.
     */
    private ArrayList<Category> mItems;

    /**
     * The resource.
     */
    private int resource;

    /**
     * The m inflater.
     */
    private LayoutInflater mInflater;

    /**
     * The default options.
     */
    private DisplayImageOptions defaultOptions;

    /**
     * The config.
     */
    private ImageLoaderConfiguration config;

    /**
     * The image loader.
     */
    private ImageLoader imageLoader;

    /**
     * Instantiates a new news adapter.
     *
     * @param context  the context
     * @param resource the resource
     * @param objects  the objects
     */
    public CategoriesAdapter(Context context, int resource, ArrayList<Category> objects) {
        init(context, resource, objects);
    }

    /**
     * Inits the.
     *
     * @param context  the context
     * @param resource the resource
     * @param objects  the objects
     */
    public void init(Context context, int resource, ArrayList<Category> objects) {
        this.mContext = context;
        this.mItems = objects;
        this.resource = resource;
        this.mInflater = LayoutInflater.from(context);

        // UNIVERSAL IMAGE LOADER SETUP
        defaultOptions = new DisplayImageOptions.Builder()
                /*.showImageOnLoading(R.drawable.newsvide)
                .showImageForEmptyUri(R.drawable.newsvide)
				.showImageOnFail(R.drawable.newsvide)*/.cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .resetViewBeforeLoading(true).build();

        config = new ImageLoaderConfiguration.Builder(mContext)
                .defaultDisplayImageOptions(defaultOptions).build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        // END - UNIVERSAL IMAGE LOADER SETUP
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
     */
    @SuppressWarnings("deprecation")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Category item = mItems.get(position);
        Holder holder;
        if (convertView == null) {
            holder = new Holder();

            // initialize the view
            convertView = mInflater.inflate(resource, parent, false);
            holder.title = (TextView) convertView.findViewById(R.id.tv1);
            holder.radioButton = (TextView) convertView.findViewById(R.id.radioButton1);
            holder.drawable = (ImageView) convertView.findViewById(R.id.drawable);


            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }


        imageLoader.displayImage(item.get_image(), holder.drawable);

        String shortDescription;


        shortDescription = Html.fromHtml(item.get_description()).toString();


        int index = 80;
        if (shortDescription.length() > index) {
            String desc_txt = shortDescription.substring(0, index);
            desc_txt = desc_txt.substring(0, desc_txt.lastIndexOf(" "));
            holder.radioButton.setText(desc_txt + " ...");
        } else {
            holder.radioButton.setText(shortDescription);
        }
        holder.title.setText(item.get_title());


        return convertView;
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getCount()
     */
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mItems.size();
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
    public Category getItem(int arg0) {
        return mItems.get(arg0);
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * The Class Holder.
     */
    class Holder {

        /**
         * The times.
         */
        public TextView title, radioButton;

        /**
         * The drawable1.
         */
        public ImageView drawable;
    }

}