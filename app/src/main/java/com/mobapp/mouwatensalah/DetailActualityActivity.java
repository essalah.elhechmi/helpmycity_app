package com.mobapp.mouwatensalah;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.mobapp.mouwatensalah.fragments.ActualityFragment;
import com.mobapp.mouwatensalah.fragments.DetailActualityFragment;
import com.mobapp.mouwatensalah.model.News;
import com.mobapp.mouwatensalah.tools.LocallyFiles;

import java.util.ArrayList;

// TODO: Auto-generated Javadoc

/**
 * The Class DetailActualityActivity. support all details news fragments
 */
public class DetailActualityActivity extends AppCompatActivity implements
        OnPageChangeListener {

    private Toolbar toolbar;                              // Declaring the Toolbar Object

    /**
     * The current position.
     */
    protected int currentPosition = 0;

    /**
     * The view pager.
     */
    private ViewPager viewPager;

    /**
     * The my page adapter.
     */
    private MyPageAdapter myPageAdapter;

    /**
     * The id.
     */
    public String id = "1";

    /**
     * The news list.
     */
    private ArrayList<News> newsList;

    /**
     * The Locally files stored into sdCard.
     */
    private LocallyFiles mlocallyFiles;

    /* (non-Javadoc)
     * @see android.support.v7.app.ActionBarActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.detail_actuality);


        // set the custom actionbar
//        toolbar = (Toolbar) findViewById(R.id.tool_bar);
//        setSupportActionBar(toolbar);

        // display the back btn
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // get the id of selected article
        id = getIntent().getStringExtra(ActualityFragment.SHARED_POSITION);
        mlocallyFiles = new LocallyFiles(this);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOnPageChangeListener(this);
        newsList = News.getListNews(DetailActualityActivity.this);


        // seeking all news without Ad
        ArrayList<DetailActualityFragment> newsFragments = new ArrayList<DetailActualityFragment>();
        int realIndex = -1;
        for (int i = 0; i < newsList.size(); i++) {
            newsFragments.add(DetailActualityFragment.getInstance(newsList
                    .get(i), this));
            realIndex++;
            if (id != null)
                if (newsList.get(i).getId().equals(id)) {
                    currentPosition = realIndex;
                }

        }

        myPageAdapter = new MyPageAdapter(getSupportFragmentManager(),
                newsFragments);
        viewPager.setAdapter(myPageAdapter);
        viewPager.setCurrentItem(currentPosition);
    }

    /**
     * The Class MyPageAdapter.
     */
    private class MyPageAdapter extends FragmentPagerAdapter {

        /**
         * The fragments.
         */
        private ArrayList<DetailActualityFragment> fragments = new ArrayList<DetailActualityFragment>();

        /**
         * Instantiates a new my page adapter.
         *
         * @param fm        the fm
         * @param fragments the fragments
         */
        public MyPageAdapter(FragmentManager fm,
                             ArrayList<DetailActualityFragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.FragmentPagerAdapter#getItem(int)
         */
        @Override
        public Fragment getItem(int position) {
            Log.d("position", "" + position);
            return this.fragments.get(position);
        }

        /* (non-Javadoc)
         * @see android.support.v4.view.PagerAdapter#getCount()
         */
        @Override
        public int getCount() {
            return this.fragments.size();
        }

    }

    /* (non-Javadoc)
     * @see android.support.v4.view.ViewPager.OnPageChangeListener#onPageScrollStateChanged(int)
     */
    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see android.support.v4.view.ViewPager.OnPageChangeListener#onPageScrolled(int, float, int)
     */
    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see android.support.v4.view.ViewPager.OnPageChangeListener#onPageSelected(int)
     */
    @Override
    public void onPageSelected(int arg0) {
        // TODO Auto-generated method stub
        id = "" + newsList.get(arg0).getId();

        currentPosition = arg0;
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.news, menu);

        return true;
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_share:
                //if (Languages.getLocalLanguage(this).equals(Const.FR))
                //new ShareSN(this).share(Communication.URL_SHARE_NEWS + id + "/" + Const.FR, getResources().getString(R.string.app_name));
                //else
                //new ShareSN(this).share(Communication.URL_SHARE_NEWS + id + "/" + Const.AR, getResources().getString(R.string.app_name));
                break;

            case android.R.id.home:
                finish();
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}
